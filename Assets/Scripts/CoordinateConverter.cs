﻿


public static class CoordinateConverter
{
	public static void indexToCoords(int index, out int x, out int y, out int z)
	{
		const int a = 1024 * 1024, b = 1024;
		z = index / a;
		index -= z * a;
		y = index / b;
		index -= y * b;
		x = index;
		
		z += 1;
	}
	
	public static void coordsToIndex(int x, int y, int z, out int index)
	{
		z -= 1;
		
		index = x + (y * 1024) + (z * 1024 * 1024);
	}
}
