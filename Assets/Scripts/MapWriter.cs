using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MapWriter {
	public enum Status
	{
		Success,
		InvalidGridDimension,
		Truncated,
		FileFailure
	}
	
	public const int MAP_ROW_SECTIONS = 8, MAP_COL_SECTIONS = 8;
	public const int MAP_SECTION_ROW_CELLS = 256, MAP_SECTION_COL_CELLS = 256;
	public static string WorkingDirectory = "";
	
	public static Status WriteTerrain(Terrain.Cell[,][] grid)
	{
		if (File.Exists(WorkingDirectory + "Heightmap.png"))
			File.Delete(WorkingDirectory + "Heightmap.png");

		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		Status status = Status.Success;
		byte[, ,] section = new byte[MAP_SECTION_COL_CELLS, MAP_SECTION_ROW_CELLS, 4];
		
		int rowSections, colSections;
		rowSections = xLength / MAP_SECTION_ROW_CELLS;
		if (xLength % MAP_SECTION_ROW_CELLS > 0)
			rowSections++;
		if (rowSections >= MAP_ROW_SECTIONS)
		{
			rowSections = MAP_ROW_SECTIONS;
			status = Status.Truncated;
		}
		colSections = yLength / MAP_SECTION_COL_CELLS;
		if (yLength % MAP_SECTION_COL_CELLS > 0)
			colSections++;
		if (colSections >= MAP_COL_SECTIONS)
		{
			colSections = MAP_COL_SECTIONS;
			status = Status.Truncated;
		}
		
		
		for (int row = 0; row < rowSections; row++)
		{
			for (int col = 0; col < colSections; col++)
			{
				for (int i = 0; i < MAP_SECTION_ROW_CELLS; i++)
				{
					for (int j = 0; j < MAP_SECTION_COL_CELLS; j++)
					{
						if ((i + row * MAP_SECTION_ROW_CELLS) >= xLength || (j + col * MAP_SECTION_COL_CELLS) >= yLength)
						{
							for (int k = 0; k < 4; k++)
								section[j, i, k] = 0x00;
							continue;
						}
						
						int iS = i + row * MAP_SECTION_ROW_CELLS;
						int jS = j + col * MAP_SECTION_COL_CELLS;
						if (grid[iS, jS] == null || grid[iS, jS].Length == 0 || grid[iS, jS][0] == null)
						{
							section[j, i, 0] = 0;
							section[j, i, 1] = 0;
							section[j, i, 2] = 0;
							section[j, i, 3] = 0;
						} else
						{
							int index = grid[iS, jS].Length-1;
							section[j, i, 0] = grid[iS, jS][index].lowerBound;
							section[j, i, 1] = grid[iS, jS][index].upperBound;
							section[j, i, 2] = grid[iS, jS][index].type;
							section[j, i, 3] = grid[iS, jS][index].slope;
						}
					}
				}
				
				string name = "Monde_" + ((col * MAP_COL_SECTIONS) + row);
				WriteSection(section, name);
			}
		}
		
		
		return status;
	}
	
	private static Status WriteSection(byte[, ,] section, string name)
	{
		Status status = Status.Success;
		
		FileStream fs;
		byte[] preFill = { 0x00, 0x00 };
		fs = File.Create(WorkingDirectory + name);
		if (!fs.CanWrite)
		{
			fs.Close();
			return Status.FileFailure;
		}
		
		fs.Write(preFill, 0, preFill.Length);
		for (int i = 0; i < section.GetLength(0); i++)
			for (int j = 0; j < section.GetLength(1); j++)
				for (int k = 0; k < section.GetLength(2); k++)
					fs.WriteByte(section[i, j, k]);
		fs.Close();
		
		return status;
	}
	
	public static bool WriteDoodads(int startingPointXPos, int startingPointYPos, int startingPointZPos, int crystalXPos, int crystalYPos, int crystalZPos)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Doodads");
		if (!fs.CanWrite)
			return false;
		
		int indexSP, indexC;
		CoordinateConverter.coordsToIndex(startingPointXPos, startingPointYPos, startingPointZPos, out indexSP);
		CoordinateConverter.coordsToIndex(crystalXPos, crystalYPos, crystalZPos, out indexC);
		
		string str = "StartingPoint " + indexSP + " Crystal " + indexC;
		char[] chars = str.ToCharArray();
		byte[] data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs.Write(data, 0, data.Length);
		fs.Close();
		return true;
	}

	public static bool WriteObjectsNew(ICollection<Level.LevelObject> forests, ICollection<Level.LevelObject> mines, ICollection<Level.LevelObject> enemies, int startingPointXPos, int startingPointYPos, int startingPointZPos)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Objets.brx");
		if (!fs.CanWrite)
			return false;
		FileStream fs2;
		fs2 = File.Create(WorkingDirectory + "gameobjects.json");
		if (!fs2.CanWrite)
			return false;


		int tempIndex, objectNumber = 0;
		bool firstObject;
		string str = "", str2 = "";


		str = ""
			+ "[Monde @test=[Array $0 $1 $2 $3]"
			+ "\n @Champignons=[Array]"
			+ "\n @Tetes=[Array]"
			+ "\n @Sacs=[Array]"
			+ "\n @Roches=[Array]"
			+ "\n @Crystals=[Array]"
			+ "\n @Autres=[Array $[BaliseAI @rayon=0 @groupeTag=[String @this=\"BaliseStartingPoint\"]";
		CoordinateConverter.coordsToIndex(startingPointXPos, startingPointYPos, startingPointZPos, out tempIndex);
		str += ""
			+ "\n@groupeID=3 @index=" + tempIndex + " @groupeDesc=[String @this=\"Custom\"]"
			+ "\n]";
		


		str2 = "[";

		// Mines
		if (mines != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in mines)
			{
				if (firstObject)
					firstObject = false;
				else
					str2 += ",";

				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				
				str2 += "\n  {"
					+ "\n    \"$id\": \"go:" + (19238+objectNumber) + "\","
					+ "\n    \"$type\": \"UnityEngine.GameObject, UnityEngine\","
					+ "\n    \"$path\": \"/Jeu/ObjetsDynamiques\","
					+ "\n    \"$assetKey\": \"ObjetsDynamiques.MineMagique\","
					+ "\n    \"name\": \"MineMagique\","
					+ "\n    \"components\": ["
					+ "\n        {"
					+ "\n            \"$type\": \"UnityEngine.Transform, UnityEngine\","
					+ "\n            \"position\": {"
					+ "\n                \"$type\": \"Brix.IO.Serialization.Json.Converters.Vector3DTO, Brix.Core\","
					+ "\n                \"x\": " + (lo.position.x) + ","
					+ "\n                \"y\": " + (lo.position.y-1) + ","
					+ "\n                \"z\": " + (lo.position.z)
					+ "\n            },"
					+ "\n            \"rotation\": {"
					+ "\n                \"$type\": \"Brix.IO.Serialization.Json.Converters.QuaternionDTO, Brix.Core\","
					+ "\n                \"x\": 0.0,"
					+ "\n                \"y\": 0.0,"
					+ "\n                \"z\": 0.0,"
					+ "\n                \"w\": 0.7071068"
					+ "\n            }"
					+ "\n        },"
					+ "\n        {"
					+ "\n            \"$type\": \"MineMagique, Assembly-UnityScript\","
					+ "\n            \"groupeVoulu\": " + (4+objectNumber)
					+ "\n        }"
					+ "\n    ]"
					+ "\n  }";
				objectNumber++;
			}
		}
		// /Mines

		// Enemies
		if (enemies != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in enemies)
			{
				if (firstObject)
					firstObject = false;

				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += ""
					+ "\n$[BaliseAI @rayon=0 @groupeTag=[String @this=\"CorruptronSpawnPoint\"]"
					+ "\n @groupeID=" + objectNumber++ + " @index=" + tempIndex + " @groupeDesc=[String @this=\"Custom\"]"
					+ "\n]";
			}
		}
		// /Enemies

		str += ""
			+ "\n$[BaliseAI @rayon=0 @groupeTag=[String @this=\"NextRound\"]"
			+ "\n @groupeID=8 @index=95920655 @groupeDesc=[String @this=\"Custom\"]"
			+ "\n]"
			+ "\n]"
			+ "\n @Echelles=[Array]"
			+ "\n]";

		str2 += "\n]\n";


		char[] chars = str.ToCharArray();
		byte[] data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs.Write(data, 0, data.Length);
		fs.Close();
		chars = str2.ToCharArray();
		data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs2.Write(data, 0, data.Length);
		fs2.Close();
		return true;
	}
	
	public static bool WriteObjects(ICollection<Level.LevelObject> forests, ICollection<Level.LevelObject> mines, ICollection<Level.LevelObject> enemies)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Objets.json");
		if (!fs.CanWrite)
			return false;
		FileStream fs2;
		fs2 = File.Create(WorkingDirectory + "gameobjects.json");
		if (!fs2.CanWrite)
			return false;
		
		
		int tempIndex, objectNumber = 0;
		bool firstObject;
		
		string str = "{\n  \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n  \"Version\": 2,\n  \"ObjectDynamiques\": {\n    \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n    \"ForetMagiques\": [";
		
		// Forests
		if (forests != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in forests)
			{
				if (firstObject)
					firstObject = false;
				else
					str += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += "\n      {\n        \"$type\": \"ForetMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
				/*if (lo.FacingDirection == UsefulDefines.Direction.YB)
					str += "0";
				else if (lo.FacingDirection == UsefulDefines.Direction.YF)
					str += "1";
				else if (lo.FacingDirection == UsefulDefines.Direction.XB)
					str += "2";
				else if (lo.FacingDirection == UsefulDefines.Direction.XF)
					str += "3";
				else*/
					str += "0"; 
				str += ",\n        \"network_id\": " + objectNumber + "\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ],\n    \"Palettes\": [],\n    \"Balises\": [";
		
		// Enemies
		if (enemies != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in enemies)
			{
				if (firstObject)
					firstObject = false;
				else
					str += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += "\n      {\n        \"$type\": \"BaliseAI, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"rayon\": " + objectNumber + ",\n        \"groupeID\": " + objectNumber + ",\n        \"groupeDesc\": \"Custom\",\n        \"groupeTag\": \"CorruptronSpawnPoint\"\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ],\n    \"MineMagiques\": [";

		string str2 = "[\n    ";
		int goid = 19238;
		// Mines
		if (mines != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in mines)
			{
				if (firstObject)
					firstObject = false;
				else
					//str += ",";
					str2 += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str2 += "{\n    \"$id\": \"go:" + goid++ + "\",\n    \"$type\": \"UnityEngine.GameObject, UnityEngine\",\n    \"$assetKey\": \"ObjetsDynamiques.MineMagique\",\n    \"name\": \"MineMagique\",\n    \"components\": [\n      {\n        \"$type\": \"MineMagique, Assembly-UnityScript\",\n        \"groupeVoulu\": " + objectNumber + "\n      },\n      {\n        \"$type\": \"VoxelTransform, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": 0\n      }\n    ]\n  }";
				//str += "\n      {\n        \"$type\": \"MineMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
				/*if (lo.FacingDirection == UsefulDefines.Direction.YB)
					str += "0";
				else if (lo.FacingDirection == UsefulDefines.Direction.YF)
					str += "1";
				else if (lo.FacingDirection == UsefulDefines.Direction.XB)
					str += "2";
				else if (lo.FacingDirection == UsefulDefines.Direction.XF)
					str += "3";
				else*/
					//str += "0";
				//str += ",\n        \"network_id\": " + objectNumber + ",\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ]\n  }\n}";
		
		char[] chars = str.ToCharArray();
		byte[] data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs.Write(data, 0, data.Length);
		fs.Close();
		chars = str2.ToCharArray();
		data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs2.Write(data, 0, data.Length);
		fs2.Close();
		return true;
	}
	
	public static bool WriteTrees(bool[,] trees, byte[,] heights)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Arbre");
		if (!fs.CanWrite)
			return false;
		
		int xBound = Math.Min(trees.GetLength(0), heights.GetLength(0));
		int yBound = Math.Min(trees.GetLength(1), heights.GetLength(1));
		int index;
		
		for (int i = 0; i < xBound; i++)
		{
			for (int j = 0; j < yBound; j++)
			{
				if (trees[i, j] == true)
				{
					CoordinateConverter.coordsToIndex(i, j, heights[i, j], out index);
					byte[] bytes = BitConverter.GetBytes(index);
					fs.Write(bytes, 0, 4);
				}
			}
		}
		fs.Close();
		
		return true;
	}
	
	public static bool ClearFolder()
	{
		for (int i = 0; i < MAP_ROW_SECTIONS * MAP_COL_SECTIONS; i++)
			File.Delete(WorkingDirectory + "Monde_" + i);
		File.Delete("Monde_Doodads");
		File.Delete("Monde_Arbre");
		File.Delete("Monde_Objets.json");
		
		return true;
	}

}
