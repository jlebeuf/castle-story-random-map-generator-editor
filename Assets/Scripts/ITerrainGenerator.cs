﻿using UnityEngine;
using System.Collections;

public interface ITerrainGenerator {

	bool BuildTerrain(Terrain.Cell[,][] grid);
}
