using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MapReader {
	public enum Status
	{
		Success,
		InvalidGridDimension,
		Truncated,
		FileFailure
	}
	
	public const int MAP_ROW_SECTIONS = 8, MAP_COL_SECTIONS = 8;
	public const int MAP_SECTION_ROW_CELLS = 256, MAP_SECTION_COL_CELLS = 256;
	public static string WorkingDirectory = "";
	
	public static Terrain.Cell[,][] ReadTerrain()
	{
		int lastXSection = 0, lastYSection = 0;
		for (int i = 0; i < MAP_ROW_SECTIONS; i++)
		{
			for (int j = 0; j < MAP_COL_SECTIONS; j++)
			{
				if (File.Exists(WorkingDirectory + "Monde_" + ((j*MAP_COL_SECTIONS)+i)))
				{
					if (i+1 > lastXSection)
						lastXSection = i+1;
					if (j+1 > lastYSection)
						lastYSection = j+1;
				}
			}
		}

		int xLength = lastXSection*MAP_SECTION_ROW_CELLS;
		int yLength = lastYSection*MAP_SECTION_COL_CELLS;
		Terrain.Cell[,][] terrain = new Terrain.Cell[xLength, yLength][];
		byte[,,] section = new byte[MAP_SECTION_COL_CELLS, MAP_SECTION_ROW_CELLS, 4];
		for (int i = 0; i < lastXSection; i++)
		{
			for (int j = 0; j < lastYSection; j++)
			{
				ReadSection(ref section, "Monde_" + ((j*MAP_COL_SECTIONS)+i));
				int xSec = 0;
				for (int xPos = i*MAP_SECTION_ROW_CELLS; xPos < (i+1)*MAP_SECTION_ROW_CELLS; xPos++)
				{
					int ySec = 0;
					for (int yPos = j*MAP_SECTION_COL_CELLS; yPos < (j+1)*MAP_SECTION_COL_CELLS; yPos++)
					{
						if (section[ySec, xSec, 2] != 0)
						{
							terrain[xPos, yPos] = new Terrain.Cell[1];
							terrain[xPos, yPos][0] = new Terrain.Cell();
							terrain[xPos, yPos][0].lowerBound = section[ySec, xSec, 0];
							terrain[xPos, yPos][0].upperBound = section[ySec, xSec, 1];
							terrain[xPos, yPos][0].type = section[ySec, xSec, 2];
							terrain[xPos, yPos][0].slope = section[ySec, xSec, 3];
						}
						ySec++;
					}
					xSec++;
				}
			}
		}

		return terrain;
	}
	
	private static Status ReadSection(ref byte[,,] section, string name)
	{
		int xLength = section.GetLength(0);
		int yLength = section.GetLength(1);

		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				section[i, j, 0] = 0x00;
				section[i, j, 1] = 0x00;
				section[i, j, 2] = 0x00;
				section[i, j, 3] = 0x00;
			}
		}
		if (!File.Exists(WorkingDirectory + name))
			return Status.FileFailure;

		byte[] file = File.ReadAllBytes(WorkingDirectory + name);
		int readLength = file.Length;
		int xPos = 0, yPos = 0;
		for (int i = 2; (i+4) < readLength; i += 4)
		{
			section[xPos, yPos, 0] = file[i+0];
			section[xPos, yPos, 1] = file[i+1];
			section[xPos, yPos, 2] = file[i+2];
			section[xPos, yPos, 3] = file[i+3];
			yPos++;
			if (yPos >= yLength)
			{
				yPos = 0;
				xPos++;
			}
		}
		if ((float)(readLength-2)%4 != 0)
			return Status.Truncated;

		return Status.Success;
	}

	public static bool ReadDoodads(out int startingPointXPos, out int startingPointYPos, out int startingPointZPos, out int crystalXPos, out int crystalYPos, out int crystalZPos)
	{
		if (!File.Exists(WorkingDirectory + "Monde_Doodads"))
		{
			startingPointXPos = 0;
			startingPointYPos = 0;
			startingPointZPos = 0;
			crystalXPos = 0;
			crystalYPos = 0;
			crystalZPos = 0;
			return false;
		}

		StreamReader file = new StreamReader(WorkingDirectory + "Monde_Doodads");
		string data = file.ReadToEnd();

		string startPosStr = "StartingPoint";
		string crystalPosStr = "Crystal";
		string indexStr;

		int startPosIndex = data.IndexOf(startPosStr);
		startPosIndex += startPosStr.Length+1;
		indexStr = "";
		while (startPosIndex < data.Length && char.IsDigit(data[startPosIndex]))
			indexStr += data[startPosIndex++];
		startPosIndex = int.Parse(indexStr);
		CoordinateConverter.indexToCoords(startPosIndex, out startingPointXPos, out startingPointYPos, out startingPointZPos);
		
		int crystalPosIndex = data.IndexOf(crystalPosStr);
		crystalPosIndex += startPosStr.Length+1;
		indexStr = "";
		while (crystalPosIndex < data.Length && char.IsDigit(data[crystalPosIndex]))
			indexStr += data[crystalPosIndex++];
		crystalPosIndex = int.Parse(indexStr);
		CoordinateConverter.indexToCoords(crystalPosIndex, out crystalXPos, out crystalYPos, out crystalZPos);

		return true;
	}
	
	public static bool WriteObjects(ICollection<Level.LevelObject> forests, ICollection<Level.LevelObject> mines, ICollection<Level.LevelObject> enemies)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Objets.json");
		if (!fs.CanWrite)
			return false;
		
		
		int tempIndex, objectNumber = 0;
		bool firstObject;
		
		string str = "{\n  \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n  \"Version\": 2,\n  \"ObjectDynamiques\": {\n    \"$type\": \"Boo.Lang.Hash, Boo.Lang\",\n    \"ForetMagiques\": [";
		
		// Forests
		if (forests != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in forests)
			{
				if (firstObject)
					firstObject = false;
				else
					str += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += "\n      {\n        \"$type\": \"ForetMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
				/*if (lo.FacingDirection == UsefulDefines.Direction.YB)
					str += "0";
				else if (lo.FacingDirection == UsefulDefines.Direction.YF)
					str += "1";
				else if (lo.FacingDirection == UsefulDefines.Direction.XB)
					str += "2";
				else if (lo.FacingDirection == UsefulDefines.Direction.XF)
					str += "3";
				else*/
				str += "0"; 
				str += ",\n        \"network_id\": " + objectNumber + "\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ],\n    \"Palettes\": [],\n    \"Balises\": [";
		
		// Enemies
		if (enemies != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in enemies)
			{
				if (firstObject)
					firstObject = false;
				else
					str += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += "\n      {\n        \"$type\": \"BaliseAI, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"rayon\": " + objectNumber + ",\n        \"groupeID\": " + objectNumber + ",\n        \"groupeDesc\": \"Custom\",\n        \"groupeTag\": \"CorruptronSpawnPoint\"\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ],\n    \"MineMagiques\": [";
		
		// Mines
		if (mines != null)
		{
			firstObject = true;
			foreach (Level.LevelObject lo in mines)
			{
				if (firstObject)
					firstObject = false;
				else
					str += ",";
				CoordinateConverter.coordsToIndex((int)lo.position.x, (int)lo.position.z, (int)lo.position.y, out tempIndex);
				str += "\n      {\n        \"$type\": \"MineMagique, Assembly-UnityScript\",\n        \"index\": " + tempIndex + ",\n        \"dir\": ";
				/*if (lo.FacingDirection == UsefulDefines.Direction.YB)
					str += "0";
				else if (lo.FacingDirection == UsefulDefines.Direction.YF)
					str += "1";
				else if (lo.FacingDirection == UsefulDefines.Direction.XB)
					str += "2";
				else if (lo.FacingDirection == UsefulDefines.Direction.XF)
					str += "3";
				else*/
				str += "0";
				str += ",\n        \"network_id\": " + objectNumber + ",\n      }";
				objectNumber++;
			}
		}
		
		str += "\n    ]\n  }\n}";
		
		char[] chars = str.ToCharArray();
		byte[] data = new byte[chars.Length];
		for (int i = 0; i < chars.Length; i++)
			data[i] = (byte)chars[i];
		fs.Write(data, 0, data.Length);
		fs.Close();
		return true;
	}
	
	public static bool WriteTrees(bool[,] trees, byte[,] heights)
	{
		FileStream fs;
		fs = File.Create(WorkingDirectory + "Monde_Arbre");
		if (!fs.CanWrite)
			return false;
		
		int xBound = Math.Min(trees.GetLength(0), heights.GetLength(0));
		int yBound = Math.Min(trees.GetLength(1), heights.GetLength(1));
		int index;
		
		for (int i = 0; i < xBound; i++)
		{
			for (int j = 0; j < yBound; j++)
			{
				if (trees[i, j] == true)
				{
					CoordinateConverter.coordsToIndex(i, j, heights[i, j], out index);
					byte[] bytes = BitConverter.GetBytes(index);
					fs.Write(bytes, 0, 4);
				}
			}
		}
		fs.Close();
		
		return true;
	}
}
