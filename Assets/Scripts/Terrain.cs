﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Terrain : MonoBehaviour {
	public class RenderSection {
		public GameObject gameObject = null;
		public Mesh mesh = null;
		public List<Vector3> vertices = new List<Vector3>();
		public List<int> triangles = new List<int>();
		public List<Vector2> uv = new List<Vector2>();
		public int squares = 0;
		public bool changed = false;
	}
	[System.Serializable]
	public class UVMapping {
		public int xPos = 0;
		public int yPos = 0;
		public float xSize = 0;
		public float ySize = 0;
	}
	public class Cell {
		public const byte LOWEST_BOUND = 0;
		public const byte HIGHEST_BOUND = 250;
		public byte lowerBound = 0;
		public byte upperBound = 0;
		public byte slope = 0;
		public byte type = 0;
	}

	public int xLength = 0;
	public int yLength = 0;
	public int xCellsPerSection = 1;
	public int yCellsPerSection = 1;
	public bool useUVs = true;
	public UVMapping xLowerTexture;
	public UVMapping xUpperTexture;
	public UVMapping yLowerTexture;
	public UVMapping yUpperTexture;
	public UVMapping zLowerTexture;
	public UVMapping zUpperTexture;
	public UVMapping treeTexture;
	public UVMapping dirtTexture;
	
	public TestTerrainGenerator.TestFillType testFillType;

	private int _xLength = 0;
	public int XLength { get { return _xLength; } }
	private int _yLength = 0;
	public int YLength { get { return _yLength; } }
	private Cell[,][] grid;
	public Cell[,][] Grid { get { return grid; } }
	private bool[,] trees = null;
	public bool[,] Trees { get { return trees; } }
	private RenderSection[,] renderSections;
	private bool globalChanged = false;

	private GameObject _gameObject;
	private Transform _transform;
	private Material[] _materials;
	private Renderer _renderer;

	// Use this for initialization
	void Start () {
		_gameObject = gameObject;
		_transform = transform;
		_renderer = GetComponent<MeshRenderer>().renderer;
		_materials = _renderer.materials;
		grid = new Cell[xLength, yLength][];
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				grid[i, j] = null;
		trees = new bool[xLength, yLength];
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				trees[i, j] = false;
		_xLength = xLength;
		_yLength = yLength;
		int xRenderSections = Mathf.CeilToInt((float)xLength/(float)xCellsPerSection);
		int yRenderSections = Mathf.CeilToInt((float)yLength/(float)yCellsPerSection);
		renderSections = new RenderSection[xRenderSections, yRenderSections];
		for (int i = 0; i < xRenderSections; i++)
		{
			for (int j = 0; j < yRenderSections; j++)
			{
				renderSections[i, j] = new RenderSection();
				renderSections[i, j].gameObject = new GameObject("TerrainRenderSection("+i+","+j+")");
				renderSections[i, j].mesh = renderSections[i, j].gameObject.AddComponent<MeshFilter>().mesh;
				renderSections[i, j].gameObject.AddComponent<MeshRenderer>().materials = _materials;
				renderSections[i, j].gameObject.AddComponent<MeshCollider>();
				renderSections[i, j].gameObject.transform.parent = _transform;
				renderSections[i, j].vertices = new List<Vector3>();
				renderSections[i, j].triangles = new List<int>();
				renderSections[i, j].uv = new List<Vector2>();
				renderSections[i, j].squares = 0;
				renderSections[i, j].changed = true;
			}
		}

		testFillTerrain();
	}
	
	// Update is called once per frame
	void Update () {
		drawMesh();
	}

	private void markRenderSectionFromGridCoordsChanged(int x, int y)
	{
		renderSections[(int)(x/xCellsPerSection - float.Epsilon), (int)(y/yCellsPerSection - float.Epsilon)].changed = true;
	}

	public void resize()
	{
		resize(xLength, yLength, 0, 0);
	}

	public void resize(int newXLength, int newYLength, int xOffset, int yOffset)
	{
		int oldXLength, oldYLength;
		_xLength = newXLength;
		_yLength = newYLength;

		Cell[,][] newGrid = new Cell[newXLength, newYLength][];
		oldXLength = grid.GetLength(0);
		oldYLength = grid.GetLength(1);
		for (int i = 0; i < newXLength; i++)
		{
			for (int j = 0; j < newYLength; j++)
			{
				int x = i+xOffset;
				int y = j+yOffset;

				if (x >= 0 && x < oldXLength && y >= 0 && y < oldYLength)
					newGrid[i, j] = grid[x, y];
				else
					newGrid[i, j] = null;
			}
		}
		grid = newGrid;

		bool[,] newTrees = new bool[newXLength, newYLength];
		oldXLength = trees.GetLength(0);
		oldYLength = trees.GetLength(1);
		for (int i = 0; i < newXLength; i++)
		{
			for (int j = 0; j < newYLength; j++)
			{
				int x = i+xOffset;
				int y = j+yOffset;
				
				if (x >= 0 && x < oldXLength && y >= 0 && y < oldYLength)
					newTrees[i, j] = trees[x, y];
				else
					newTrees[i, j] = false;
			}
		}
		trees = newTrees;

		int newXRenderSections = Mathf.CeilToInt((float)_xLength/(float)xCellsPerSection);
		int newYRenderSections = Mathf.CeilToInt((float)_yLength/(float)yCellsPerSection);
		int oldXRenderSections = renderSections.GetLength(0);
		int oldYRenderSections = renderSections.GetLength(1);
		int iterationXRenderSections = Mathf.Max(newXRenderSections, oldXRenderSections);
		int iterationYRenderSections = Mathf.Max(newYRenderSections, oldYRenderSections);
		RenderSection[,] newRenderSections = new RenderSection[newXRenderSections, newYRenderSections];
		for (int i = 0; i < iterationXRenderSections; i++)
		{
			for (int j = 0; j < iterationYRenderSections; j++)
			{
				if (i >= newXRenderSections || j >= newYRenderSections)
				{
					// Shrinking, dispose of old renderSections objects
					GameObject.Destroy(renderSections[i, j].gameObject);
				} else
				if (i < oldXRenderSections && j < oldYRenderSections)
				{
					// Within old renderSections bounds, reuse old objects
					newRenderSections[i, j] = renderSections[i, j];
				} else
				{
					// Growing past old bounds, make new objects
					newRenderSections[i, j] = new RenderSection();
					newRenderSections[i, j].gameObject = new GameObject("TerrainRenderSection("+i+","+j+")");
					newRenderSections[i, j].gameObject.layer = _gameObject.layer;
					newRenderSections[i, j].mesh = newRenderSections[i, j].gameObject.AddComponent<MeshFilter>().mesh;
					newRenderSections[i, j].gameObject.AddComponent<MeshRenderer>().materials = _materials;
					newRenderSections[i, j].gameObject.AddComponent<MeshCollider>();
					newRenderSections[i, j].gameObject.transform.parent = _transform;
					newRenderSections[i, j].vertices = new List<Vector3>();
					newRenderSections[i, j].triangles = new List<int>();
					newRenderSections[i, j].uv = new List<Vector2>();
					newRenderSections[i, j].squares = 0;
					newRenderSections[i, j].changed = true;
				}
			}
		}
		renderSections = newRenderSections;

		globalChanged = true;
	}

	public bool toggleTree(int x, int y, int z)
	{
		bool next = !trees[x, z];
		trees[x, z] = next;
		markRenderSectionFromGridCoordsChanged(x, z);
		return next;
	}

	public void clearTrees()
	{
		for (int i = 0; i < trees.GetLength(0); i++)
			for (int j = 0; j < trees.GetLength(1); j++)
				trees[i, j] = false;

		globalChanged = true;
	}

	public void clearTerrain()
	{
		for (int i = 0; i < grid.GetLength(0); i++)
			for (int j = 0; j < grid.GetLength(1); j++)
				grid[i, j] = null;
		clearTrees();

		globalChanged = true;
	}

	public void testFillTerrain()
	{
		new TestTerrainGenerator().BuildTerrain(grid, testFillType);

		globalChanged = true;
	}

	public void toggleTerreType(int x, int y, int z)
	{
		if (x < 0 || x >= grid.GetLength(0) || y < 0 || y >= grid.GetLength(1))
			return;
		
		for (int i = 0; i < grid[x, y].Length; i++)
		{
			if (z >= grid[x, y][i].lowerBound && z <= grid[x, y][i].upperBound)
			{
				switch (grid[x, y][i].type)
				{
				case 0x02:
					grid[x, y][i].type = 0x04;
					break;
				case 0x04:
					grid[x, y][i].type = 0x02;
					break;
				}
			}
		}

		markRenderSectionFromGridCoordsChanged(x, y);
	}

	public void terreGrow(int x, int y, int z)
	{
		if (x < 0 || x >= grid.GetLength(0) || y < 0 || y >= grid.GetLength(1))
			return;

		if (grid[x, y] == null || grid[x, y].Length < 1)
			grid[x, y] = new Cell[1];
		if (grid[x, y][0] == null)
		{
			grid[x, y][0] = new Cell();
			grid[x, y][0].type = 0x02;
			grid[x, y][0].slope = 0x01;
		}

		if (z > grid[x, y][0].upperBound)
			grid[x, y][0].upperBound = (byte)z;
		else if (z < grid[x, y][0].lowerBound)
			grid[x, y][0].lowerBound = (byte)z;

		renderSections[x/xCellsPerSection, y/yCellsPerSection].changed = true;
	}

	public void terreShrink(int x, int y, int z, bool undercut)
	{
		if (x < 0 || x >= grid.GetLength(0) || y < 0 || y >= grid.GetLength(1) ||
		    grid[x, y] == null || grid[x, y].Length < 1 || grid[x, y][0] == null)
			return;

		if (undercut)
		{
			if (z <= grid[x, y][0].upperBound && z > grid[x, y][0].lowerBound)
			{
				grid[x, y][0].lowerBound = (byte)z;
				renderSections[x/xCellsPerSection, y/yCellsPerSection].changed = true;
			}
			if (grid[x, y][0].upperBound == grid[x, y][0].lowerBound ||
			    grid[x, y][0].upperBound == grid[x, y][0].lowerBound+1)
				grid[x, y] = null;
		} else
		{
			if (z < grid[x, y][0].upperBound && z >= grid[x, y][0].lowerBound)
			{
				grid[x, y][0].upperBound = (byte)z;
				renderSections[x/xCellsPerSection, y/yCellsPerSection].changed = true;
			}
			if (grid[x, y][0].upperBound == grid[x, y][0].lowerBound ||
			    grid[x, y][0].upperBound == grid[x, y][0].lowerBound+1)
				grid[x, y] = null;
		}
	}

	public bool buildTerrain(ITerrainGenerator generator)
	{
		bool status = generator.BuildTerrain(grid);

		globalChanged = true;
		return status;
	}

	public bool setTerrain(Cell[,][] newGrid)
	{
		grid = newGrid;
		resize(newGrid.GetLength(0), newGrid.GetLength(1), 0, 0);
		return true;
	}

	private void drawMesh()
	{
		int xSections = renderSections.GetLength(0);
		int ySections = renderSections.GetLength(1);
		
		for (int i = 0; i < xSections; i++)
		{
			for (int j = 0; j < ySections; j++)
			{
				if (globalChanged == true || renderSections[i, j].changed == true)
				{
					renderSections[i, j].mesh.Clear();
					renderSections[i, j].vertices.Clear();
					renderSections[i, j].triangles.Clear();
					renderSections[i, j].uv.Clear();
					renderSections[i, j].squares = 0;

					int xMax = Mathf.Min((i+1)*xCellsPerSection, grid.GetLength(0));
					int yMax = Mathf.Min((j+1)*yCellsPerSection, grid.GetLength(1));

					for (int x = i*xCellsPerSection; x < xMax; x++)
					{
						for (int y = j*yCellsPerSection; y < yMax; y++)
						{
							drawCell(renderSections[i, j], x, y, xLowerTexture, xUpperTexture, yLowerTexture, yUpperTexture, zLowerTexture, zUpperTexture);
						}
					}

					renderSections[i, j].mesh.vertices = renderSections[i, j].vertices.ToArray();
					renderSections[i, j].mesh.triangles = renderSections[i, j].triangles.ToArray();
					if (useUVs)
						renderSections[i, j].mesh.uv = renderSections[i, j].uv.ToArray();
					renderSections[i, j].mesh.Optimize();
					renderSections[i, j].mesh.RecalculateNormals();
					renderSections[i, j].gameObject.GetComponent<MeshCollider>().sharedMesh = null;
					renderSections[i, j].gameObject.GetComponent<MeshCollider>().sharedMesh = renderSections[i, j].mesh;
					renderSections[i, j].changed = false;
				}
			}
		}

		globalChanged = false;
	}

	private void drawCell(RenderSection section, int x, int y, UVMapping xLower, UVMapping xUpper, UVMapping yLower, UVMapping yUpper, UVMapping zLower, UVMapping zUpper)
	{
		if (grid[x, y] == null)
			return;

		float heightLower;
		float heightUpper;
		bool xl = ((x-1) >= 0);
		bool xu = ((x+1) < _xLength);
		bool yl = ((y-1) >= 0);
		bool yu = ((y+1) < _yLength);

		for (int i = 0; i < grid[x, y].Length; i++)
		{
			heightUpper = grid[x, y][i].upperBound;
			heightLower = grid[x, y][i].lowerBound;

			// YUpper
			section.vertices.Add(new Vector3(x, heightUpper, y+1));
			section.vertices.Add(new Vector3(x+1, heightUpper, y+1));
			section.vertices.Add(new Vector3(x+1, heightUpper, y));
			section.vertices.Add(new Vector3(x, heightUpper, y));
			section.triangles.Add((section.squares*4)+0);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+3);
			if (useUVs)
			{
				if (trees[x, y] == true)
				{
					section.uv.Add(new Vector2((treeTexture.xPos+0)*treeTexture.xSize, (treeTexture.yPos+1)*treeTexture.ySize));
					section.uv.Add(new Vector2((treeTexture.xPos+1)*treeTexture.xSize, (treeTexture.yPos+1)*treeTexture.ySize));
					section.uv.Add(new Vector2((treeTexture.xPos+1)*treeTexture.xSize, (treeTexture.yPos+0)*treeTexture.ySize));
					section.uv.Add(new Vector2((treeTexture.xPos+0)*treeTexture.xSize, (treeTexture.yPos+0)*treeTexture.ySize));
				} else if (grid[x, y][i].type == 0x04)
				{
					section.uv.Add(new Vector2((dirtTexture.xPos+0)*dirtTexture.xSize, (dirtTexture.yPos+1)*dirtTexture.ySize));
					section.uv.Add(new Vector2((dirtTexture.xPos+1)*dirtTexture.xSize, (dirtTexture.yPos+1)*dirtTexture.ySize));
					section.uv.Add(new Vector2((dirtTexture.xPos+1)*dirtTexture.xSize, (dirtTexture.yPos+0)*dirtTexture.ySize));
					section.uv.Add(new Vector2((dirtTexture.xPos+0)*dirtTexture.xSize, (dirtTexture.yPos+0)*dirtTexture.ySize));
				}else
				{
					section.uv.Add(new Vector2((yUpper.xPos+0)*yUpper.xSize, (yUpper.yPos+1)*yUpper.ySize));
					section.uv.Add(new Vector2((yUpper.xPos+1)*yUpper.xSize, (yUpper.yPos+1)*yUpper.ySize));
					section.uv.Add(new Vector2((yUpper.xPos+1)*yUpper.xSize, (yUpper.yPos+0)*yUpper.ySize));
					section.uv.Add(new Vector2((yUpper.xPos+0)*yUpper.xSize, (yUpper.yPos+0)*yUpper.ySize));
				}
			}
			section.squares++;

			// YLower
			section.vertices.Add(new Vector3(x, heightLower, y+1));
			section.vertices.Add(new Vector3(x+1, heightLower, y+1));
			section.vertices.Add(new Vector3(x+1, heightLower, y));
			section.vertices.Add(new Vector3(x, heightLower, y));
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+0);
			if (useUVs)
			{
				section.uv.Add(new Vector2((yLower.xPos+0)*yLower.xSize, (yLower.yPos+1)*yLower.ySize));
				section.uv.Add(new Vector2((yLower.xPos+1)*yLower.xSize, (yLower.yPos+1)*yLower.ySize));
				section.uv.Add(new Vector2((yLower.xPos+1)*yLower.xSize, (yLower.yPos+0)*yLower.ySize));
				section.uv.Add(new Vector2((yLower.xPos+0)*yLower.xSize, (yLower.yPos+0)*yLower.ySize));
			}
			section.squares++;

			// XLower
			section.vertices.Add(new Vector3(x, heightUpper, y));
			section.vertices.Add(new Vector3(x, heightUpper, y+1));
			section.vertices.Add(new Vector3(x, heightLower, y+1));
			section.vertices.Add(new Vector3(x, heightLower, y));
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+0);
			if (useUVs)
			{
				section.uv.Add(new Vector2((xLower.xPos+0)*xLower.xSize, (xLower.yPos+1)*xLower.ySize));
				section.uv.Add(new Vector2((xLower.xPos+1)*xLower.xSize, (xLower.yPos+1)*xLower.ySize));
				section.uv.Add(new Vector2((xLower.xPos+1)*xLower.xSize, (xLower.yPos+0)*xLower.ySize));
				section.uv.Add(new Vector2((xLower.xPos+0)*xLower.xSize, (xLower.yPos+0)*xLower.ySize));
			}
			section.squares++;

			// XUpper
			section.vertices.Add(new Vector3(x+1, heightUpper, y+1));
			section.vertices.Add(new Vector3(x+1, heightUpper, y));
			section.vertices.Add(new Vector3(x+1, heightLower, y));
			section.vertices.Add(new Vector3(x+1, heightLower, y+1));
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+0);
			if (useUVs)
			{
				section.uv.Add(new Vector2((xUpper.xPos+0)*xUpper.xSize, (xUpper.yPos+1)*xUpper.ySize));
				section.uv.Add(new Vector2((xUpper.xPos+1)*xUpper.xSize, (xUpper.yPos+1)*xUpper.ySize));
				section.uv.Add(new Vector2((xUpper.xPos+1)*xUpper.xSize, (xUpper.yPos+0)*xUpper.ySize));
				section.uv.Add(new Vector2((xUpper.xPos+0)*xUpper.xSize, (xUpper.yPos+0)*xUpper.ySize));
			}
			section.squares++;

			// ZLower
			section.vertices.Add(new Vector3(x+1, heightUpper, y));
			section.vertices.Add(new Vector3(x, heightUpper, y));
			section.vertices.Add(new Vector3(x, heightLower, y));
			section.vertices.Add(new Vector3(x+1, heightLower, y));
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+0);
			if (useUVs)
			{
				section.uv.Add(new Vector2((zLower.xPos+0)*zLower.xSize, (zLower.yPos+1)*zLower.ySize));
				section.uv.Add(new Vector2((zLower.xPos+1)*zLower.xSize, (zLower.yPos+1)*zLower.ySize));
				section.uv.Add(new Vector2((zLower.xPos+1)*zLower.xSize, (zLower.yPos+0)*zLower.ySize));
				section.uv.Add(new Vector2((zLower.xPos+0)*zLower.xSize, (zLower.yPos+0)*zLower.ySize));
			}
			section.squares++;

			// ZUpper
			section.vertices.Add(new Vector3(x, heightUpper, y+1));
			section.vertices.Add(new Vector3(x+1, heightUpper, y+1));
			section.vertices.Add(new Vector3(x+1, heightLower, y+1));
			section.vertices.Add(new Vector3(x, heightLower, y+1));
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+2);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+3);
			section.triangles.Add((section.squares*4)+1);
			section.triangles.Add((section.squares*4)+0);
			if (useUVs)
			{
				section.uv.Add(new Vector2((zUpper.xPos+0)*zUpper.xSize, (zUpper.yPos+1)*zUpper.ySize));
				section.uv.Add(new Vector2((zUpper.xPos+1)*zUpper.xSize, (zUpper.yPos+1)*zUpper.ySize));
				section.uv.Add(new Vector2((zUpper.xPos+1)*zUpper.xSize, (zUpper.yPos+0)*zUpper.ySize));
				section.uv.Add(new Vector2((zUpper.xPos+0)*zUpper.xSize, (zUpper.yPos+0)*zUpper.ySize));
			}
			section.squares++;
		}
	}
}
