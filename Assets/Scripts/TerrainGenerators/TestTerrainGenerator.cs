﻿using UnityEngine;
using System.Collections;

public class TestTerrainGenerator : ITerrainGenerator {

	public enum TestFillType
	{
		Diamond,
		Bowl,
		RandomGrid,
		GridOverGrid,
		LayerTest,
		Flat
	}

	public bool BuildTerrain(Terrain.Cell[,][] grid)
	{
		return BuildTerrain(grid, TestFillType.Flat);
	}

	public bool BuildTerrain(Terrain.Cell[,][] grid, TestFillType testFillType)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);

		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				grid[i, j] = null;


		if (testFillType == TestFillType.Diamond)
		{
			// Diamond
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].lowerBound = (byte)(i + j);
					grid[i, j][0].upperBound = (byte)(i + j + 1);
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		} else
		if (testFillType == TestFillType.Bowl)
		{
			// Bowl
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].lowerBound = 0;
					grid[i, j][0].upperBound = (byte)(0.05f*(Mathf.Pow(i-(xLength/2), 2) + Mathf.Pow(j-(yLength/2), 2)));
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		} else
		if (testFillType == TestFillType.RandomGrid)
		{
			// RandomGrid
			new NoiseTerrainGenerator().BuildTerrain(grid);
		} else
		if (testFillType == TestFillType.GridOverGrid)
		{
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					grid[i, j] = new Terrain.Cell[5];
					for (int k = 0; k < 5; k++)
					{
						grid[i, j][k] = new Terrain.Cell();
						grid[i, j][k].lowerBound = (byte)(k*5);
						grid[i, j][k].upperBound = (byte)(k*5);
						grid[i, j][k].type = 0x02;
						grid[i, j][k].slope = 0x01;
					}
				}
			}
		} else
		if (testFillType == TestFillType.LayerTest)
		{
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					byte height = (byte)((float)(i+j)/(xLength+yLength) * 250);
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].lowerBound = height;
					grid[i, j][0].upperBound = (byte)(height+2);
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		} else
		if (testFillType == TestFillType.Flat)
		{
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					byte height = (byte)(byte.MaxValue/2);
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].upperBound = height;
					grid[i, j][0].lowerBound = (byte)(height-8);
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		}

		return true;
	}
}
