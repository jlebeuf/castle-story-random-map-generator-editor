﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class OldProceduralTerrainGenerator : ITerrainGenerator {

	private interface GraphComponent {};
	private class GraphNode : GraphComponent
	{
		public int x, y, size;
		public byte z;
		public List<GraphComponent> edges = new List<GraphComponent>();
	}
	private class GraphEdgeNode : GraphComponent
	{
		public int x, y;
		public byte z;
		public GraphComponent prevNode, nextNode; // GraphNode or GraphEdgeNode, check type
	}
	
	ushort maxIslands = 10;
	ushort maxIslandPaths = 3;
	byte minimumIslandHeight = 50;
	byte maximumIslandHeight = 75;
	ushort minimumInterIslandHeightVariation = 0;
	ushort maximumInterIslandHeightVariation = 25;
	ushort minimumInterIslandLateralDistance = 10;
	ushort maximumInterIslandLateralDistance = 75;
	ushort minimumIslandSize = 20;
	ushort maximumIslandSize = 40;
	ushort minimumIslandSizeVariation = 15;
	ushort maximumislandSizeVariation = 25;
	ushort minimumPathLength = 50;
	ushort maximumPathLength = 150;
	
	System.Random rand = new System.Random();
	
	public bool BuildTerrain(Terrain.Cell[,][] terrain)
	{
		int xLength = terrain.GetLength(0);
		int yLength = terrain.GetLength(1);

		if (xLength < 512 || yLength < 512)
			return false;

		// Clear beforehand
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				terrain[i, j] = null;
		
		/*
             * Make flat island in middle of grid and add to a queue
             * For each island in queue
             *   Pick x edges to grow from
             *     For each edge
             *       Grow path out to random point and sprinkle liberally with midpoint displacement
             *       Create island at end and add to queue
             *       Pick path type (sides rounded off, walls, etc.)
             * 
             */
		
		NoiseGenerator noise = new NoiseGenerator();
		
		GraphNode rootNode = makeIslandGraph(xLength, yLength);
		List<GraphNode> islandsToDraw = new List<GraphNode>();
		List<GraphNode> islandsDrawn = new List<GraphNode>();
		
		islandsToDraw.Add(rootNode);
		
		while (islandsToDraw.Count > 0)
		{
			GraphNode node = islandsToDraw[0];
			islandsToDraw.RemoveAt(0);
			
			foreach (GraphNode n in node.edges)
				if (!islandsDrawn.Contains(n) && !islandsToDraw.Contains(n))
					islandsToDraw.Add(n);
			
			DrawIsland(terrain, node);
			islandsDrawn.Add(node);
		}
		
		return false;
	}
	
	private GraphNode makeIslandGraph(int terrainXLength, int terrainYLength)
	{
		ushort averageInterIslandDistance = (ushort)(minimumInterIslandLateralDistance + (maximumInterIslandLateralDistance - minimumInterIslandLateralDistance));
		ushort averageIslandSize = (ushort)(minimumIslandSize + (maximumIslandSize - minimumIslandSize));
		ushort averageIslandSizeVariation = (ushort)(minimumIslandSizeVariation + (maximumislandSizeVariation - minimumIslandSizeVariation));
		ushort averagePathLength = (ushort)(minimumPathLength + (maximumPathLength - minimumPathLength));
		
		Queue<GraphNode> nodesToProcess = new Queue<GraphNode>();
		Queue<GraphNode> nodesCreated = new Queue<GraphNode>();
		
		GraphNode rootNode = new GraphNode();
		rootNode.size = rand.Next(minimumIslandSize, maximumIslandSize);
		rootNode.x = terrainXLength / 2;
		rootNode.y = terrainYLength / 2;
		//rootNode.z = (byte)(minimumIslandHeight + (maximumIslandHeight - minimumIslandHeight));
		rootNode.z = minimumIslandHeight;
		nodesToProcess.Enqueue(rootNode);
		
		while (nodesToProcess.Count > 0)
		{
			if ((nodesCreated.Count + nodesToProcess.Count) >= maxIslands)
			{
				while (nodesToProcess.Count > 0)
					nodesCreated.Enqueue(nodesToProcess.Dequeue());
				continue;
			}
			
			GraphNode node = nodesToProcess.Dequeue();
			
			int paths = rand.Next(Math.Min(maxIslandPaths, maxIslands - nodesCreated.Count - nodesToProcess.Count)) - node.edges.Count;
			
			for (int i = 0; i < paths; i++)
			{
				double angle = rand.NextDouble() * 2 * Math.PI;
				int distance = rand.Next(minimumInterIslandLateralDistance, maximumInterIslandLateralDistance);
				GraphNode nextNode = new GraphNode();
				nextNode.size = rand.Next(minimumIslandSize, maximumIslandSize);
				nextNode.x = (int)(node.x + Math.Cos(angle) * (node.size + nextNode.size + 2 * averageIslandSizeVariation + rand.Next(minimumInterIslandLateralDistance, maximumInterIslandLateralDistance)));
				nextNode.y = (int)(node.y + Math.Sin(angle) * (node.size + nextNode.size + 2 * averageIslandSizeVariation + rand.Next(minimumInterIslandLateralDistance, maximumInterIslandLateralDistance)));
				//nextNode.z = (byte)rand.Next(minimumIslandHeight, maximumIslandHeight);
				nextNode.z = minimumIslandHeight;
				
				if ((nextNode.x - maximumIslandSize - 10 < 0) || (nextNode.x + maximumIslandSize + 10 >= terrainXLength) ||
				    (nextNode.y - maximumIslandSize - 10 < 0) || (nextNode.y + maximumIslandSize + 10 >= terrainYLength))
					continue;
				
				bool conflict = false;
				Queue<GraphNode> temp = new Queue<GraphNode>();
				foreach (GraphNode n in nodesCreated)
					temp.Enqueue(n);
				foreach (GraphNode n in nodesToProcess)
					if (!temp.Contains(n))
						temp.Enqueue(n);
				foreach (GraphNode n in temp)
				{
					if ((Math.Sqrt(Math.Pow(n.x - nextNode.x, 2) + Math.Pow(n.y - nextNode.y, 2)) - (n.size + nextNode.size + 2 * averageIslandSizeVariation)) < minimumInterIslandLateralDistance)
					{
						conflict = true;
						break;
					}
				}
				
				if (conflict)
					continue;
				
				node.edges.Add(nextNode);
				nextNode.edges.Add(node);
				
				nodesToProcess.Enqueue(nextNode);
			}
		}
		
		return rootNode;
	}
	
	private void DrawIsland(Terrain.Cell[,][] terrain, GraphNode node)
	{
		NoiseGenerator noise = new NoiseGenerator(6, 2, 0.045, 0.3);

		int xLength = terrain.GetLength(0);
		int yLength = terrain.GetLength(1);
		
		int x = node.x;
		int y = node.y;
		int xLast = x, yLast = y;
		int xMin = x, xMax = x, yMin = y, yMax = y;
		double angleStep = Math.PI / (Math.Sqrt(Math.Pow(xLength, 2) + Math.Pow(yLength, 2)));
		int distanceVariation = rand.Next(minimumIslandSizeVariation, maximumislandSizeVariation);
		
		for (double angle = 0; angle < 2 * Math.PI; angle += angleStep)
		{
			int xEnd = (int)(x + Math.Cos(angle) * (node.size + distanceVariation));
			int yEnd = (int)(y + Math.Sin(angle) * (node.size + distanceVariation));
			double variation = noise.Noise(xEnd, yEnd);
			xEnd = (int)(x + Math.Cos(angle) * (node.size + (distanceVariation * variation)));
			yEnd = (int)(y + Math.Sin(angle) * (node.size + (distanceVariation * variation)));
			
			if (xLast == xEnd && yLast == yEnd)
				continue;
			xMin = (xEnd < xMin) ? xEnd : xMin;
			xMax = (xEnd > xMax) ? xEnd : xMax;
			yMin = (yEnd < yMin) ? yEnd : yMin;
			yMax = (yEnd > yMax) ? yEnd : yMax;
			
			int x1, x2, y1, y2;
			if (xLast < xEnd)
			{
				x1 = xLast;
				y1 = yLast;
				x2 = xEnd;
				y2 = yEnd;
			}
			else
			{
				x1 = xEnd;
				y1 = yEnd;
				x2 = xLast;
				y2 = yLast;
			}
			double slope = (y1 - y2 == 0) ? 0 : (x1 - x2) / (y1 - y2);
			
			for (int i = x1; i < x2; i++)
			{
				int yPos = (int)((x1 - i) * slope + y1);
				if (terrain[i, yPos+0] == null || terrain[i, yPos+0].Length > 0)
					terrain[i, yPos+0] = new Terrain.Cell[1];
				if (terrain[i, yPos+0][0] == null)
					terrain[i, yPos+0][0] = new Terrain.Cell();
				if (terrain[i, yPos+1] == null || terrain[i, yPos+1].Length > 0)
					terrain[i, yPos+1] = new Terrain.Cell[1];
				if (terrain[i, yPos+1][0] == null)
					terrain[i, yPos+1][0] = new Terrain.Cell();

				terrain[i, yPos + 0][0].upperBound = node.z;
				terrain[i, yPos + 1][0].upperBound = node.z;
				terrain[i, yPos + 0][0].lowerBound = (byte)(node.z - 3);
				terrain[i, yPos + 1][0].lowerBound = (byte)(node.z - 3);
				terrain[i, yPos + 0][0].type = 0x02;
				terrain[i, yPos + 1][0].type = 0x02;
				terrain[i, yPos + 0][0].slope = 0x01;
				terrain[i, yPos + 1][0].slope = 0x01;
			}

			if (terrain[xEnd, yEnd] == null || terrain[xEnd, yEnd].Length == 0)
				terrain[xEnd, yEnd] = new Terrain.Cell[1];
			if (terrain[xEnd, yEnd][0] == null)
				terrain[xEnd, yEnd][0] = new Terrain.Cell();
			terrain[xEnd, yEnd][0].upperBound = node.z;
			terrain[xEnd, yEnd][0].lowerBound = (byte)(node.z - 3);
			terrain[xEnd, yEnd][0].type = 0x02;
			terrain[xEnd, yEnd][0].slope = 0x01;
			
			xLast = xEnd;
			yLast = yEnd;
		}
		
		for (int i = xMin; i <= xMax; i++)
		{
			for (int j = yMin; j <= yMax; j++)
			{
				if (terrain[i, j] == null || terrain[i, j].Length == 0 || terrain[i, j][0] == null || terrain[i, j][0].upperBound == 0)
					continue;
				
				bool fill = false;
				for (int k = yMax; k > j; k--)
				{
					if (fill)
					{
						if (terrain[i, k] == null || terrain[i, k].Length == 0)
							terrain[i, k] = new Terrain.Cell[1];
						if (terrain[i, k][0] == null)
							terrain[i, k][0] = new Terrain.Cell();
						terrain[i, k][0].upperBound = node.z;
						terrain[i, k][0].lowerBound = (byte)(node.z - 3);
						terrain[i, k][0].type = 0x02;
						terrain[i, k][0].slope = 0x01;
					}
					else if (terrain[i, k] != null && terrain[i, k].Length > 0 && terrain[i, k][0] != null && terrain[i, k][0].upperBound != 0)
						fill = true;
				}
				goto breakToI;
			}
		breakToI:
				continue;
		}
	}
}
