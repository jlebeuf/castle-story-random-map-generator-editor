﻿using UnityEngine;
using System.Collections;

public class NoiseMaskedNoiseTerrainGenerator : ITerrainGenerator {
	
	public const int DEFAULT_OCTAVES = 1;
	public const double DEFAULT_AMPLITUDE = 10;
	public const double DEFAULT_FREQUENCY = 0.04;
	public const double DEFAULT_PERSISTENCE = 0.15;
	public const byte DEFAULT_UPPERBOUND = 0x4c;
	public const byte DEFAULT_LOWERBOUND = 0x3f;
	public const byte DEFAULT_VARIATION = 0x04;
	public const byte DEFAULT_THICKNESS = 0x03;
	public const byte DEFAULT_HIGHPASS = 0x48;
	public const bool DEFAULT_HIGHPASSACTIVE = false;
	public const byte DEFAULT_LOWPASS = 0x49;
	public const bool DEFAULT_LOWPASSACTIVE = true;
	public int octaves;
	public double amplitude;
	public double frequency;
	public double persistence;
	/// <summary>
	/// The height the highest point/level should be
	/// </summary>
	public byte upperBound;
	/// <summary>
	/// The height the lowest point/level should be
	/// </summary>
	public byte lowerBound;
	/// <summary>
	/// The minimum amount of cells below each surface cell
	/// </summary>
	public byte thickness;
	/// <summary>
	/// The distance between the highest and lowest height points... I think; It's 0230 and I should have documented when originally writing
	/// </summary>
	public byte variation;
	/// <summary>
	/// Nulls cells with surface lower than this
	/// </summary>
	public byte lowPassFilter;
	/// <summary>
	/// Whether to apply the low pass filter
	/// </summary>
	public bool lowPassFilterActive;
	/// <summary>
	/// Nulls cells with surface higher than this
	/// </summary>
	public byte highPassFilter;
	/// <summary>
	/// Whether to apply the high pass filter
	/// </summary>
	public bool highPassFilterActive;
	private NoiseGenerator noiseGeneratorBase;
	private NoiseGenerator noiseGeneratorMask;
	
	public NoiseMaskedNoiseTerrainGenerator()
	{
		this.upperBound = DEFAULT_UPPERBOUND;
		this.lowerBound = DEFAULT_LOWERBOUND;
		this.thickness = DEFAULT_THICKNESS;
		this.variation = DEFAULT_VARIATION;
		this.octaves = DEFAULT_OCTAVES;
		this.amplitude = DEFAULT_AMPLITUDE;
		this.frequency = DEFAULT_FREQUENCY;
		this.persistence = DEFAULT_PERSISTENCE;
		this.lowPassFilter = 0;
		this.lowPassFilterActive = false;
		this.highPassFilter = 0;
		this.highPassFilterActive = false;
		noiseGeneratorBase = new NoiseGenerator(octaves, amplitude, frequency, persistence);
		noiseGeneratorMask = new NoiseGenerator(octaves, amplitude, frequency, persistence);
	}
	
	public NoiseMaskedNoiseTerrainGenerator(byte upperBound, byte thickness, byte variation)
	{
		this.upperBound = upperBound;
		this.lowerBound = byte.MaxValue;
		this.thickness = thickness;
		this.variation = variation;
		this.octaves = DEFAULT_OCTAVES;
		this.amplitude = DEFAULT_AMPLITUDE;
		this.frequency = DEFAULT_FREQUENCY;
		this.persistence = DEFAULT_PERSISTENCE;
		this.lowPassFilter = 0;
		this.lowPassFilterActive = false;
		this.highPassFilter = 0;
		this.highPassFilterActive = false;
		noiseGeneratorBase = new NoiseGenerator(octaves, amplitude, frequency, persistence);
		noiseGeneratorMask = new NoiseGenerator(octaves, amplitude, frequency, persistence);
	}
	
	public NoiseMaskedNoiseTerrainGenerator(byte upperBound, byte lowerBound, byte thickness, byte variation)
	{
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.thickness = thickness;
		this.variation = variation;
		this.octaves = DEFAULT_OCTAVES;
		this.amplitude = DEFAULT_AMPLITUDE;
		this.frequency = DEFAULT_FREQUENCY;
		this.persistence = DEFAULT_PERSISTENCE;
		this.lowPassFilter = 0;
		this.lowPassFilterActive = false;
		this.highPassFilter = 0;
		this.highPassFilterActive = false;
		noiseGeneratorBase = new NoiseGenerator(octaves, amplitude, frequency, persistence);
		noiseGeneratorMask = new NoiseGenerator(octaves, amplitude, frequency, persistence);
	}
	
	public NoiseMaskedNoiseTerrainGenerator(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence)
	{
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.thickness = thickness;
		this.variation = variation;
		this.octaves = octaves;
		this.amplitude = amplitude;
		this.frequency = frequency;
		this.persistence = persistence;
		this.lowPassFilter = 0;
		this.lowPassFilterActive = false;
		this.highPassFilter = 0;
		this.highPassFilterActive = false;
		noiseGeneratorBase = new NoiseGenerator(octaves, amplitude, frequency, persistence);
		noiseGeneratorMask = new NoiseGenerator(octaves, amplitude, frequency, persistence);
	}
	
	public void setParameters(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence)
	{
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.thickness = thickness;
		this.variation = variation;
		this.octaves = octaves;
		this.amplitude = amplitude;
		this.frequency = frequency;
		this.persistence = persistence;
	}
	
	public void setParameters(byte upperBound, byte lowerBound, byte thickness, byte variation, int octaves, double amplitude, double frequency, double persistence, byte lowPassFilter, byte highPassFilter)
	{
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.thickness = thickness;
		this.variation = variation;
		this.octaves = octaves;
		this.amplitude = amplitude;
		this.frequency = frequency;
		this.persistence = persistence;
		this.lowPassFilter = lowPassFilter;
		this.lowPassFilterActive = true;
		this.highPassFilter = highPassFilter;
		this.highPassFilterActive = true;
	}
	
	public bool BuildTerrain(Terrain.Cell[,][] terrain)
	{
		int xLength = terrain.GetLength(0);
		int yLength = terrain.GetLength(1);

		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				terrain[i, j] = null;
		
		if (upperBound > Terrain.Cell.HIGHEST_BOUND)
			upperBound = Terrain.Cell.HIGHEST_BOUND;
		if (lowerBound >= upperBound)
			lowerBound = (byte)(upperBound - 2);
		byte midpoint = (byte)(lowerBound + ((upperBound - lowerBound) / 2));
		
		noiseGeneratorBase.generateNoise(octaves, amplitude, frequency, persistence*2);
		noiseGeneratorMask.generateNoise(octaves, amplitude, frequency/2, persistence/2);
		noiseGeneratorMask.Seed *= noiseGeneratorMask.r.Next();
		
		// Heights
		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (noiseGeneratorMask.Noise(i, j) != 1)
				{
					terrain[i, j] = new Terrain.Cell[1];
					terrain[i, j][0] = new Terrain.Cell();
					int height = midpoint;
					float offset = (float)noiseGeneratorBase.Noise(i, j);
					offset *= (float)variation;
					height += (int)offset;
					terrain[i, j][0].upperBound = (byte)((height < 0) ? 0 : (height < byte.MaxValue) ? height : byte.MaxValue);
					
					// Types, Slopes
					terrain[i, j][0].type = 0x02;
					terrain[i, j][0].slope = 0x01;
				}
			}
		}
		if (lowPassFilterActive)
			ApplyLowPass(terrain);
		if (highPassFilterActive)
			ApplyHighPass(terrain);
		
		// Lows
		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (terrain[i, j] == null || terrain[i, j].Length == 0 || terrain[i, j][0] == null)
					continue;

				if (terrain[i, j][0].upperBound == 0 || terrain[i, j][0].upperBound == 1)
				{
					terrain[i, j][0].lowerBound = 0;
					continue;
				}
				
				byte lowest = byte.MaxValue;
				
				for (int r = i - 2; r < i + 2; r++)
				{
					for (int c = j - 2; c < j + 2; c++)
					{
						if (r >= 0 && r < xLength && c >= 0 && c < yLength
						    && terrain[r, c] != null && terrain[r, c].Length != 0 && terrain[r, c][0] != null
						    && terrain[r, c][0].upperBound != 0 && terrain[r, c][0].upperBound < lowest)
							lowest = terrain[r, c][0].upperBound;
					}
				}
				
				lowest -= thickness;
				if (lowest < 0)
					lowest = 0;
				else if (lowest < lowerBound)
					lowest = lowerBound;
				if (lowest >= terrain[i, j][0].upperBound)
					lowest = (byte)(terrain[i, j][0].upperBound - 0x01);
				
				terrain[i, j][0].lowerBound = lowest;
			}
		}
		
		return true;
	}
	
	private void ApplyLowPass(Terrain.Cell[,][] terrain)
	{
		int xLength = terrain.GetLength(0);
		int yLength = terrain.GetLength(1);
		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (terrain[i, j] == null || terrain[i, j].Length == 0 || terrain[i, j][0] == null)
					continue;
				if (terrain[i, j][0].upperBound < lowPassFilter)
				{
					terrain[i, j][0].upperBound = 0;
					terrain[i, j][0].type = 0;
					terrain[i, j][0].slope = 0;
				}
			}
		}
	}
	
	private void ApplyHighPass(Terrain.Cell[,][] terrain)
	{
		int xLength = terrain.GetLength(0);
		int yLength = terrain.GetLength(1);
		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (terrain[i, j] == null || terrain[i, j].Length == 0 || terrain[i, j][0] == null)
					continue;
				if (terrain[i, j][0].upperBound > highPassFilter)
				{
					terrain[i, j][0].upperBound = 0;
					terrain[i, j][0].type = 0;
					terrain[i, j][0].slope = 0;
				}
			}
		}
	}
}
