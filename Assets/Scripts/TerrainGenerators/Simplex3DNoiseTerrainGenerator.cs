﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Simplex3DNoiseTerrainGenerator : ITerrainGenerator {

	public const byte DEFAULT_LOWER_BOUND = 0x20;
	public const byte DEFAULT_UPPER_BOUND = 0x80;
	public const byte MINIMUM_THICKNESS = 0x03;
	public const byte EDGE_FALLOFF = 0x03;
	public const float BOTTOM_SLOPE = 1.25f;
	private byte lowerBound = DEFAULT_LOWER_BOUND;
	private byte upperBound = DEFAULT_UPPER_BOUND;
	private bool useRandomSeed = true;
	private System.Random rand = new System.Random();
	private enum Direction {
		IUP,
		IDOWN,
		JUP,
		JDOWN
	}
	private class ASNode : IComparable<ASNode>, IEquatable<ASNode>
	{
		public int x;
		public int y;
		public double fCost;
		public double gCost;
		public double hCost;
		public ASNode parent;

		public int CompareTo(ASNode other)
		{
			return (int)((fCost - other.fCost)*10000);
		}
		public bool Equals(ASNode other)
		{
			if (x == other.x && y == other.y)
				return true;
			else
				return false;
		}
	}
	private class GraphNode {
		public class Link {
			public GraphNode node;
			public float distance;
			public Link() {}
			public Link(GraphNode gn, float dist) { node = gn; distance = dist; }
		}
		public int xPos;
		public int yPos;
		public Link[] links;
		public GraphNode() {}
		public GraphNode(int xPosition, int yPosition) { xPos = xPosition; yPos = yPosition; }
	}

	public bool BuildTerrain(Terrain.Cell[,][] grid)
	{
		float totalBuildTimeStart = Time.realtimeSinceStartup;
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		float tempTime1, tempTime2;

		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				grid[i, j] = null;

		Debug.Log("Making graph");
		tempTime1 = Time.realtimeSinceStartup;
		GraphNode root = makeGraph(xLength, yLength, rand.Next(3, 6), 1.25f);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Graph made, time: " + (tempTime2-tempTime1));
		Queue<GraphNode> nodesToProcess = new Queue<GraphNode>();
		Queue<GraphNode> nodesProcessed = new Queue<GraphNode>();
		nodesToProcess.Enqueue(root);
		while (nodesToProcess.Count > 0)
		{
			GraphNode current = nodesToProcess.Dequeue();
			nodesProcessed.Enqueue(current);
			Debug.Log("Making island");
			tempTime1 = Time.realtimeSinceStartup;
			int curXMin = Math.Max(0, current.xPos-xLength/5);
			int curXMax = Math.Min(xLength-1, current.xPos+xLength/5);
			int curX = curXMin + (curXMax-curXMin)/2;
			int curYMin = Math.Max(0, current.yPos-yLength/5);
			int curYMax = Math.Min(yLength-1, current.yPos+yLength/5);
			int curY = curYMin + (curYMax-curYMin)/2;
			makeIslandMetaballs(grid, curXMin, curXMax,
			                    curYMin, curYMax, lowerBound, upperBound);
			tempTime2 = Time.realtimeSinceStartup;
			Debug.Log("Island made, time: " + (tempTime2-tempTime1));
			for (int i = 0; i < current.links.Length; i++)
			{
				if (!nodesProcessed.Contains(current.links[i].node))
				{
					int nextXMin = Math.Max(0, current.links[i].node.xPos-xLength/5);
					int nextXMax = Math.Min(xLength-1, current.links[i].node.xPos+xLength/5);
					int nextX = nextXMin + (nextXMax-nextXMin)/2;
					int nextYMin = Math.Max(0, current.links[i].node.yPos-yLength/5);
					int nextYMax = Math.Min(yLength-1, current.links[i].node.yPos+yLength/5);
					int nextY = nextYMin + (nextYMax-nextYMin)/2;
					Debug.Log("Making path");
					tempTime1 = Time.realtimeSinceStartup;
					makePath(grid, curX, curY, nextX, nextY, upperBound, 10);
					tempTime2 = Time.realtimeSinceStartup;
					Debug.Log("Path made, time: " + (tempTime2-tempTime1));
				}
				if (!nodesProcessed.Contains(current.links[i].node))
					nodesToProcess.Enqueue(current.links[i].node);
			}

		}

		
		Debug.Log("Filling holes");
		tempTime1 = Time.realtimeSinceStartup;
		bool filled = fillHoles(grid, 1);
		tempTime2 = Time.realtimeSinceStartup;
		if (filled)
			Debug.Log("Holes filled, time: " + (tempTime2-tempTime1));
		else
			Debug.Log("No holes filled, time: " + (tempTime2-tempTime1));
		
		Debug.Log("Calculating edge distances");
		tempTime1 = Time.realtimeSinceStartup;
		int[,] dist = calculateEdgeDistances(grid);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Edge distances calculated, time: " + (tempTime2-tempTime1));

		Debug.Log("Rounding off edges");
		tempTime1 = Time.realtimeSinceStartup;
		roundEdges(grid, dist);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Edges rounded off, time: " + (tempTime2-tempTime1));
		
		Debug.Log("Equalizing lowers");
		tempTime1 = Time.realtimeSinceStartup;
		equalizeLowers(grid);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Lowers equalized, time: " + (tempTime2-tempTime1));

		Debug.Log("Applying lows");
		tempTime1 = Time.realtimeSinceStartup;
		applyLows(grid, dist);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Lows applied, time: " + (tempTime2-tempTime1));

		Debug.Log("Applying height noise");
		tempTime1 = Time.realtimeSinceStartup;
		applyHeightNoise(grid, dist);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Height noise applied, time: " + (tempTime2-tempTime1));

		/*
		Debug.Log("Reapplying lows");
		tempTime1 = Time.realtimeSinceStartup;
		applyLows(grid, dist);
		tempTime2 = Time.realtimeSinceStartup;
		Debug.Log("Lows reapplied, time: " + (tempTime2-tempTime1));
		*/

		/*
		Vector2[] islands = new Vector2[5];
		islands[0] = new Vector2(xLength/2, yLength/2);
		//islands[1] = new Vector2(xLength/4, yLength/4);
		//islands[2] = new Vector2(3*xLength/4, yLength/4);
		//islands[3] = new Vector2(xLength/4, 3*yLength/4);
		//islands[4] = new Vector2(3*xLength/4, 3*yLength/4);
		for (int i = 1; i < islands.Length; i++)
			islands[i] = new Vector2(rand.Next(0, xLength), rand.Next(0, yLength));

		for (int i = 0; i < islands.Length; i++)
		{
			int x = (int)islands[i].x;
			int y = (int)islands[i].y;
			makePath(grid, x, y, x, y, upperBound, 10);
		}

		for (int i = 0; i < islands.Length; i++)
			for (int j = 0; j < islands.Length; j++)
				if (i != j)
					makePath(grid, (int)islands[i].x, (int)islands[i].y, (int)islands[j].x, (int)islands[j].y, upperBound, 5);
		//*/


		/*
		makeIsland(grid, 0, (xLength/2)-1, 0, (yLength/2)-1, lowerBound, upperBound);
		makeIsland(grid, (xLength/4), (xLength*3/4)-1, (yLength/4), (yLength*3/4)-1, lowerBound, upperBound);
		makeIsland(grid, xLength/2, xLength, yLength/2, yLength, lowerBound, upperBound);
		*/
		//makeIsland(grid, 0, 512, 231, 281, lowerBound, upperBound);
		//makeIsland(grid, 231, 281, 0, 512, lowerBound, upperBound);

		/*for (int i = -3; i <= 3; i++)
			makeIsland(grid, 181+(i*30), 351+(i*30), 181, 331, lowerBound, upperBound);*/

		//makeIsland(grid, 0, xLength-1, 0, yLength-1, lowerBound, upperBound);

		float totalBuildTimeEnd = Time.realtimeSinceStartup;
		Debug.Log("BuildTerrain finished, time: " + (totalBuildTimeEnd-totalBuildTimeStart));

		return true;
	}

	private bool fillHoles(Terrain.Cell[,][] grid, int size)
	{
		// Returns whether filled a hole
		bool status = false;

		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);

		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0] != null)
					continue;

				bool xL = false, xU = false, yL = false, yU = false;
				byte xLL = 0, xLU = 0, xUL = 0, xUU = 0, yLL = 0, yLU = 0, yUL = 0, yUU = 0;
				for (int k = i-1; k >= Math.Max(0, i-size); k--)
				{
					if (k >= 0 && grid[k, j] != null && grid[k, j].Length > 0 && grid[k, j][0] != null)
					{
						xL = true;
						xLU = grid[k, j][0].upperBound;
						xLL = grid[k, j][0].lowerBound;
						goto xLBreak;
					}
				}
			xLBreak:
				for (int k = i+1; k <= i+size; k++)
				{
					if (k < xLength && grid[k, j] != null && grid[k, j].Length > 0 && grid[k, j][0] != null)
					{
						xU = true;
						xUU = grid[k, j][0].upperBound;
						xUL = grid[k, j][0].lowerBound;
						goto xUBreak;
					}
				}
			xUBreak:
				for (int k = j-1; k >= Math.Max(0, j-size); k--)
				{
					if (k >= 0 && grid[i, k] != null && grid[i, k].Length > 0 && grid[i, k][0] != null)
					{
						yL = true;
						yLU = grid[i, k][0].upperBound;
						yLL = grid[i, k][0].lowerBound;
						goto yLBreak;
					}
				}
			yLBreak:
				for (int k = j+1; k <= j+size; k++)
				{
					if (k < yLength && grid[i, k] != null && grid[i, k].Length > 0 && grid[i, k][0] != null)
					{
						yU = true;
						yUU = grid[i, k][0].upperBound;
						yUL = grid[i, k][0].lowerBound;
						goto yUBreak;
					}
				}
			yUBreak:

				byte xLAve = 0, xUAve = 0, yLAve = 0, yUAve = 0, uAve = 0, lAve = 0;
				bool xFill = false, yFill = false;
				if (xL && xU)
				{
					xLAve = (byte)((int)(xLL+xUL)/2);
					xUAve = (byte)((int)(xLU+xUU)/2);
					xFill = true;
				}
				if (yL && yU)
				{
					yLAve = (byte)((int)(yLL+yUL)/2);
					yUAve = (byte)((int)(yLU+yUU)/2);
					yFill = true;
				}

				if (xFill && yFill)
				{
					uAve = (byte)((int)(xUAve+yUAve)/2);
					lAve = (byte)((int)(xLAve+yLAve)/2);
				}
				else if (xFill)
				{
					uAve = xUAve;
					lAve = xLAve;
				}
				else // yFill
				{
					uAve = yUAve;
					lAve = yLAve;
				}

				if (xFill || yFill)
				{
					fillCell(grid, i, j, uAve, lAve);
					status = true;
				}
			}
		}

		return status;
	}

	private int[,] calculateEdgeDistances(Terrain.Cell[,][] grid)
	{
		// Created distance grid is 1 and greater for filled cell
		//  and -1 or less for non filled cell distance to nearest
		//  edge (filled and non filled boundry)


		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		
		int[,] dist = new int[xLength, yLength];
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				dist[i, j] = 0;
		bool[,] done = new bool[xLength, yLength];
		int edges = 0;
		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (i == 0 || i == xLength-1 || j == 0 || j == yLength-1)
				{
					done[i, j] = true;
					if (grid[i, j] == null || grid[i, j].Length < 1 || grid[i, j][0] == null)
						dist[i, j] = 0;
					else
					{
						dist[i, j] = 1;
						edges++;
					}
				}
				else
				{
					if (grid[i, j] == null || grid[i, j].Length < 1 || grid[i, j][0] == null)
					{
						done[i, j] = true;
						dist[i, j] = 0;
					} else
					{
						done[i, j] = false;
						dist[i, j] = int.MaxValue;
					}
				}
			}
		}
		
		int nextDist = 1;
		if (edges == (2*xLength + 2*yLength - 4))
			nextDist++;
		bool changed = true;
		while (changed)
		{
			changed = false;
			
			for (int i = 1; i < xLength-1; i++)
			{
				for (int j = 1; j < yLength-1; j++)
				{
					if (done[i, j])
						continue;
					if (grid[i, j] == null || grid[i, j].Length < 1 || grid[i, j][0] == null)
					{
						done[i, j] = true;
						dist[i, j] = 0;
						changed = true;
						continue;
					}
					
					int lowestDist = int.MaxValue;
					
					if (dist[i-1, j-1] < lowestDist)
						lowestDist = dist[i-1, j-1];
					
					// Ymid
					if (dist[i-1, j] < lowestDist)
						lowestDist = dist[i-1, j];
					// /Ymid
					
					if (dist[i-1, j+1] < lowestDist)
						lowestDist = dist[i-1, j+1];
					
					// Xmid
					
					if (dist[i, j-1] < lowestDist)
						lowestDist = dist[i, j-1];
					
					if (dist[i, j+1] < lowestDist)
						lowestDist = dist[i, j+1];
					// /Xmid
					
					if (dist[i+1, j-1] < lowestDist)
						lowestDist = dist[i+1, j-1];
					
					// Ymid
					if (dist[i+1, j] < lowestDist)
						lowestDist = dist[i+1, j];
					// /Ymid
					
					if (dist[i+1, j+1] < lowestDist)
						lowestDist = dist[i+1, j+1];
					
					if (++lowestDist == nextDist)
					{
						dist[i, j] = lowestDist;
						done[i, j] = true;
						changed = true;
					}
				}
			}
			nextDist++;
		}

		return dist;
	}
	
	private void roundEdges(Terrain.Cell[,][] grid, int[,] dist)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		float randF = (float)rand.NextDouble();

		for (int d = 1; d <= EDGE_FALLOFF; d++)
		{
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					if (dist[i, j] != d)
						continue;

					byte lowest = byte.MaxValue;
					for (int r = i-1; r <= i+1; r++)
					{
						for (int c = j-1; c <= j+1; c++)
						{
							if (r >= 0 && r < xLength && c >= 0 && c < yLength && !(r == i && c == j) && (r == i || c == j))
							{
								if (grid[r, c] != null && grid[r, c].Length > 0 && grid[r, c][0] != null)
								{
									if (grid[r, c][0].upperBound < lowest)
										lowest = grid[r, c][0].upperBound;
								} else
									lowest = byte.MinValue;
							}
						}
					}

					float prob = SimplexNoise.Generate((int)1, i, j, randF)/1;
					prob = Math.Abs(prob)/2 + 0.5f;
					// Prob now from 0.5 to 1
					int dropoffDist = (int)(EDGE_FALLOFF * prob);
					if (dist[i, j] > 0 && dist[i, j] <= dropoffDist && grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0] != null)
					{
						byte drop = (byte)(1*(EDGE_FALLOFF-dist[i, j]));
						if (grid[i, j][0].upperBound-drop < lowest)
							drop -= (byte)(lowest-(grid[i, j][0].upperBound-drop));
						grid[i, j][0].upperBound -= drop;
						grid[i, j][0].lowerBound -= drop;
						grid[i, j][0].type = 0x04;
					}
				}
			}
		}
	}

	private void equalizeLowers(Terrain.Cell[,][] grid)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);

		byte min = byte.MaxValue;
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0] != null && grid[i, j][0].lowerBound < min)
					min = grid[i, j][0].lowerBound;
		
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0] != null && min < grid[i, j][0].lowerBound)
					grid[i, j][0].lowerBound = min;
	}

	// Returns created edge distance grid
	private int[,] applyLows(Terrain.Cell[,][] grid, int[,] dist)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);

		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0] != null)
				{
					/*
					int newLow = grid[i, j][0].upperBound - MINIMUM_THICKNESS - (int)(BOTTOM_SLOPE*dist[i, j]) - ((rand.Next(0, 2) > 0)?1:0);
					if (newLow <= 0 || newLow > grid[i, j][0].upperBound - MINIMUM_THICKNESS)
						newLow = grid[i, j][0].upperBound - MINIMUM_THICKNESS;
					grid[i, j][0].lowerBound = (byte)Math.Min(newLow, grid[i, j][0].lowerBound);
					*/
					grid[i, j][0].lowerBound = (byte)(grid[i, j][0].lowerBound - (BOTTOM_SLOPE*dist[i, j]) - ((rand.Next(0, 2) > 0)?1:0));
				}
			}
		}

		return dist;
	}

	private void applyHeightNoise(Terrain.Cell[,][] grid, int[,] dist)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		int variation = 10;
		float randF = (float)rand.NextDouble();

		for (int i = 0; i < xLength; i++)
		{
			for (int j = 0; j < yLength; j++)
			{
				if (grid[i, j] != null && grid[i, j].Length > 0)
				{
					float noise = SimplexNoise.Generate((int)3, 1f*((float)i/xLength), 1f*((float)j/yLength), randF)/3;
					/*
					if (dist[i, j] > 15)
						noise = 0;
					else if (noise > -0.5f && noise < 0.5f)
						noise = 0;
					else if (noise < -0.5f)
						noise += 0.5f;
					else
						noise -= 0.5f;
					noise *= 2;
					byte newUpper = (byte)(grid[i, j][0].upperBound + noise*variation);
					grid[i, j][0].upperBound = newUpper;
					if (newUpper <= grid[i, j][0].lowerBound + 1 + MINIMUM_THICKNESS)
						grid[i, j][0].lowerBound = (byte)(newUpper-2-MINIMUM_THICKNESS);
					*/
					byte adj = (byte)(variation*noise);
					grid[i, j][0].upperBound += adj;
					grid[i, j][0].lowerBound += adj;
				}
			}
		}
	}

	private GraphNode makeGraph(int xLength, int yLength, int islandCount, float multiLinkPercentage)
	{
		// Redo
		// Start with island in center
		// For some amount create islands at random angles outword from center
		// Connect islands to neighbors with distance within multiLinkPercentage

		if (islandCount <= 0)
			return null;

		int minimumIslandDistance = Math.Min(xLength, yLength)/4;
		int maximumIslandDistance = minimumIslandDistance*2;
		double minimumAngle = (62.5/180.0 * Math.PI);

		GraphNode root = new GraphNode();
		root.xPos = xLength/2;
		root.yPos = yLength/2;
		root.links = new GraphNode.Link[islandCount-1];
		double[] angles = new double[root.links.Length];
		for (int i = 0; i < root.links.Length; i++)
		{
			double angle;
			bool conflict;
			int tries = 0;
			do
			{
				conflict = false;
				angle = rand.NextDouble() * 2 * Math.PI;
				for (int j = 0; j < i; j++)
				{
					double angleDiff = Math.PI - Math.Abs(Math.Abs(angle-angles[j]) - Math.PI);
					if (angleDiff < minimumAngle)
						conflict = true;
				}
				tries++;
			} while (conflict && tries < 50);
			angles[i] = angle;

			int randDistance = rand.Next(minimumIslandDistance);
			int x = (int)(root.xPos + Math.Cos(angle) * (rand.Next(minimumIslandDistance, maximumIslandDistance)));
			if (x < 0)
				x = 0;
			else if (x >= xLength)
				x = xLength-1;
			int y = (int)(root.yPos + Math.Sin(angle) * (rand.Next(minimumIslandDistance, maximumIslandDistance)));
			if (y < 0)
				y = 0;
			else if (y >= yLength)
				y = yLength-1;
			float dist = (float)Math.Sqrt(Math.Pow(root.xPos-x, 2) + Math.Pow(root.yPos-y, 2));
			GraphNode newNode = new GraphNode(x, y);
			newNode.links = new GraphNode.Link[1];
			newNode.links[0] = new GraphNode.Link(root, dist);
			root.links[i] = new GraphNode.Link(newNode, dist);
		}

		return root;
	}

	private bool graphNodesConnected(GraphNode startNode, GraphNode endNode)
	{
		Queue<GraphNode> unexplored = new Queue<GraphNode>();
		Queue<GraphNode> explored = new Queue<GraphNode>();
		unexplored.Enqueue(startNode);
		while (unexplored.Count > 0)
		{
			GraphNode current = unexplored.Dequeue();
			if (current == endNode)
				return true;
			explored.Enqueue(current);

			if (current.links != null)
			{
				for (int i = 0; i < current.links.Length; i++)
				{
					if (!explored.Contains(current.links[i].node))
					    unexplored.Enqueue(current.links[i].node);
				}
			}
		}
		return false;
	}

	private void makePath(Terrain.Cell[,][] grid, int startX, int startY, int endX, int endY, byte height, int thickness)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		int heapCutoff = (int)(1.25*Math.Sqrt(Math.Pow(startX-endX, 2)+Math.Pow(startY-endY, 2)));
		float randomness = (float)rand.NextDouble();
		double heuristicFactor = 3;

		BinaryHeap<ASNode> heap = new BinaryHeap<ASNode>();
		bool[,] inHeap = new bool[xLength, yLength];
		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				inHeap[i, j] = false;
		ASNode root = new ASNode();
		ASNode final = null;
		root.x = startX;
		root.y = startY;
		root.parent = null;
		root.gCost = 0;
		root.hCost = Math.Sqrt(Math.Pow(root.x-endX, 2)+Math.Pow(root.y-endY, 2));
		root.fCost = root.gCost + root.hCost;
		heap.Add(root);
		inHeap[root.x, root.y] = true;

		bool debugStop = false;
		while (heap.Count > 0 && !debugStop)
		{
			if (heap.Count > 2*heapCutoff)
				heuristicFactor = 1;
			else if (heap.Count > heapCutoff)
				heuristicFactor /= 2;

			ASNode current = heap.Remove();
			if (current.x >= endX-1 && current.x <= endX+1 && current.y >= endY-1 && current.y <= endY+1)
			{
				final = current;
				break;
			}
			inHeap[current.x, current.y] = false;

			ASNode next = null; // Node next to be added
			ASNode prev = null; // Node previously added
			bool	XL = (current.x-2 >= 0)? true : false,
					XU = (current.x+2 < xLength)? true : false,
					YL = (current.y-2 >= 0)? true : false,
					YU = (current.y+2 < yLength)? true : false;
			if (XL)
			{
				if (YL)
				{
					next = new ASNode();
					next.x = current.x-2;
					next.y = current.y-2;
					next.parent = current;
					next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
					next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
					next.fCost = next.gCost + next.hCost;
					if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
					{
						if (next.gCost < prev.gCost)
							heap.Add(next);
						else
							heap.Add(prev);
					} else
						heap.Add(next);
					inHeap[next.x, next.y] = true;
				}
				next = new ASNode();
				next.x = current.x-2;
				next.y = current.y;
				next.parent = current;
				next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
				next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
				next.fCost = next.gCost + next.hCost;
				if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
				{
					if (next.gCost < prev.gCost)
						heap.Add(next);
					else
						heap.Add(prev);
				} else
					heap.Add(next);
				inHeap[next.x, next.y] = true;
				if (YU)
				{
					next = new ASNode();
					next.x = current.x-2;
					next.y = current.y+2;
					next.parent = current;
					next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
					next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
					next.fCost = next.gCost + next.hCost;
					if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
					{
						if (next.gCost < prev.gCost)
							heap.Add(next);
						else
							heap.Add(prev);
					} else
						heap.Add(next);
					inHeap[next.x, next.y] = true;
				}
			}
			if (XU)
			{
				if (YL)
				{
					next = new ASNode();
					next.x = current.x+2;
					next.y = current.y-2;
					next.parent = current;
					next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
					next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
					next.fCost = next.gCost + next.hCost;
					if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
					{
						if (next.gCost < prev.gCost)
							heap.Add(next);
						else
							heap.Add(prev);
					} else
						heap.Add(next);
					inHeap[next.x, next.y] = true;
				}
				next = new ASNode();
				next.x = current.x+2;
				next.y = current.y;
				next.parent = current;
				next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
				next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
				next.fCost = next.gCost + next.hCost;
				if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
				{
					if (next.gCost < prev.gCost)
						heap.Add(next);
					else
						heap.Add(prev);
				} else
					heap.Add(next);
				inHeap[next.x, next.y] = true;
				if (YU)
				{
					next = new ASNode();
					next.x = current.x+2;
					next.y = current.y+2;
					next.parent = current;
					next.gCost = current.gCost + heuristicFactor*Math.Abs(SimplexNoise.Generate((int)5, (float)next.x, (float)next.y, randomness));
					next.hCost = Math.Sqrt(Math.Pow(next.x-endX, 2)+Math.Pow(next.y-endY, 2));
					next.fCost = next.gCost + next.hCost;
					if (inHeap[next.x, next.y] && heap.RemoveEqual(next, out prev))
					{
						if (next.gCost < prev.gCost)
							heap.Add(next);
						else
							heap.Add(prev);
					} else
						heap.Add(next);
					inHeap[next.x, next.y] = true;
				}
			}
		}

		while (final != null)
		{
			int xMin = Math.Max(final.x-thickness, 0);
			int xMax = Math.Min(final.x+thickness, xLength);
			int yMin = Math.Max(final.y-thickness, 0);
			int yMax = Math.Min(final.y+thickness, yLength);
			for (int i = xMin; i < xMax; i++)
				for (int j = yMin; j < yMax; j++)
					if (grid[i, j] == null)
						fillCell(grid, i, j, height);
			final = final.parent;
		}
	}

	private void makeIsland(Terrain.Cell[,][] grid, int xStart, int xEnd, int yStart, int yEnd, byte lower, byte upper)
	{
		int iSize = xEnd - xStart;
		int jSize = yEnd - yStart;
		double iRand = rand.NextDouble();
		double jRand = rand.NextDouble();
		
		Direction dir = Direction.IUP;
		int iStart = xStart + (iSize / 2);
		int jStart = yStart + (jSize / 2);
		if ((float)iSize % 2f == 0)
			iStart--;
		if ((float)jSize % 2f == 0)
			jStart--;
		int i = iStart;
		int j = jStart;
		int iMin = iStart - 1;
		int iMax = iStart + 1;
		int jMin = jStart - 1;
		int jMax = jStart + 1;
		
		while (i >= xStart && i <= xEnd && j >= yStart && j <= yEnd)
		{
			// Do stuff here

			bool xL = (i-1 >= xStart)? true : false;
			bool xU = (i+1 <= xEnd)? true : false;
			bool yL = (j-1 >= yStart)? true : false;
			bool yU = (j+1 <= yEnd)? true : false;
			int neighbors = 0;
			if (xL && yL && grid[i-1, j-1]!=null)
				neighbors++;
			if (xL && grid[i-1, j]!=null)
				neighbors++;
			if (xL && yU && grid[i-1, j+1]!=null)
				neighbors++;
			if (yL && grid[i, j-1]!=null)
				neighbors++;
			if (yU && grid[i, j+1]!=null)
				neighbors++;
			if (xU && yL && grid[i+1, j-1]!=null)
				neighbors++;
			if (xU && grid[i+1, j]!=null)
				neighbors++;
			if (xU && yU && grid[i+1, j+1]!=null)
				neighbors++;

			for (byte k = lower; k <= upper; k++)
			{
				double lateralDistance = Math.Pow(
					Math.Pow((i-iStart), 2) + Math.Pow((j-jStart), 2),
					0.5
					);
				double xDist = Math.Abs((float)(i-iStart)/(float)iSize);
				double yDist = Math.Abs((float)(j-jStart)/(float)jSize);
				double latF = 1 - (Math.Pow(xDist, 2)+Math.Pow(yDist, 2));
				double vertF = 1 - Math.Pow((double)(upper-k)/(double)(upper-lower), 3);
				double noise = Math.Abs(SimplexNoise.Generate(5, (float)iRand+i*0.001f, (float)jRand+j*0.001f, k*0.001f)/5f);

				if (latF+vertF > 1.95 || (noise > 0.25 && neighbors >= 2))
					fillCell(grid, i, j, k);
			}

			// End do stuff

			if (dir == Direction.IUP)
			{
				if (i < iMax)
				{
					i++;
					if (i > xEnd)
					{
						i--;
						j = jMax++;
						dir = Direction.IDOWN;
						continue;
					}
				}
				if (i == iMax)
				{
					iMax++;
					dir = Direction.JUP;
				}
			} else
			if (dir == Direction.JUP)
			{
				if (j < jMax)
				{
					j++;
					if (j > yEnd)
					{
						j--;
						i = iMin--;
						dir = Direction.JDOWN;
						continue;
					}
				}
				if (j == jMax)
				{
					jMax++;
					dir = Direction.IDOWN;
				}
			} else
			if (dir == Direction.IDOWN)
			{
				if (i > iMin)
				{
					i--;
					if (i < xStart)
					{
						i++;
						j = jMin--;
						dir = Direction.IUP;
						continue;
					}
				}
				if (i == iMin)
				{
					iMin--;
					dir = Direction.JDOWN;
				}
			} else
			if (dir == Direction.JDOWN)
			{
				if (j > jMin)
				{
					j--;
					if (j < yStart)
					{
						j++;
						i = iMax++;
						dir = Direction.JUP;
						continue;
					}
				}
				if (j == jMin)
				{
					jMin--;
					dir = Direction.IUP;
				}
			}
		}
	}

	private void fillCell(Terrain.Cell[,][] grid, int xPos, int yPos, byte height)
	{
		fillCell(grid, xPos, yPos, height, (byte)(height-MINIMUM_THICKNESS));
	}

	private void fillCell(Terrain.Cell[,][] grid, int xPos, int yPos, byte height, byte lower)
	{
		if (grid[xPos, yPos] == null)
		{
			grid[xPos, yPos] = new Terrain.Cell[1];
			grid[xPos, yPos][0] = new Terrain.Cell();
			grid[xPos, yPos][0].upperBound = height;
			grid[xPos, yPos][0].lowerBound = lower;
			grid[xPos, yPos][0].type = 0x02;
			grid[xPos, yPos][0].slope = 0x01;
		} else
		{
			// Single column for now
			if (grid[xPos, yPos][0].upperBound < height)
				grid[xPos, yPos][0].upperBound = (byte)height;
			if (grid[xPos, yPos][0].lowerBound > height)
				grid[xPos, yPos][0].lowerBound = (byte)height;
			
			// Need to account for merging overlap!
			/*foreach (Terrain.Cell cell in grid[xPos, yPos])
			{
				if (cell.upperBound >= height && cell.lowerBound <= height)
				{
					goto nextCell;
				} else
					if (cell.lowerBound-1 == height)
				{
					cell.lowerBound--;
					goto nextCell;
				} else
					if (cell.upperBound+1 == height)
				{
					cell.upperBound++;
					goto nextCell;
				}
			}
			Terrain.Cell[] newCellArray = new Terrain.Cell[grid[xPos, yPos].Length+1];
			for (int i2 = 0; i2 < grid[xPos, yPos].Length; i2++)
				newCellArray[i2] = grid[xPos, yPos][i2];
			newCellArray[newCellArray.Length-1] = new Terrain.Cell();
			newCellArray[newCellArray.Length-1].lowerBound = (byte)height;
			newCellArray[newCellArray.Length-1].upperBound = (byte)height;
			newCellArray[newCellArray.Length-1].type = 0x02;
			newCellArray[newCellArray.Length-1].slope = 0x01;
			grid[xPos, yPos] = newCellArray;//*/
		}
	}

	private void makeIslandMetaballs(Terrain.Cell[,][] grid, int xMin, int xMax, int yMin, int yMax, byte lower, byte upper)
	{
		int xLength = xMax-xMin;
		int yLength = yMax-yMin;
		int numBalls = 10;
		float gooeyness = 1f;
		float iso = numBalls*2f/5f;
		Vector3[] balls = new Vector3[numBalls];
		float rad = Math.Min(xMax-xMin, yMax-yMin)/7.5f;
		/*
		balls[0] = new Vector3(xMin + (xMax-xMin)*1/2, yMin + (yMax-yMin)*1/2, rad);
		balls[1] = new Vector3(xMin + (xMax-xMin)*1/4, yMin + (yMax-yMin)*1/4, rad);
		balls[2] = new Vector3(xMin + (xMax-xMin)*3/4, yMin + (yMax-yMin)*1/4, rad);
		balls[3] = new Vector3(xMin + (xMax-xMin)*1/4, yMin + (yMax-yMin)*3/4, rad);
		balls[4] = new Vector3(xMin + (xMax-xMin)*3/4, yMin + (yMax-yMin)*3/4, rad);
		balls[5] = new Vector3(xMin + (xMax-xMin)*1/2, yMin + (yMax-yMin)*1/4, rad);
		balls[6] = new Vector3(xMin + (xMax-xMin)*1/2, yMin + (yMax-yMin)*3/4, rad);
		balls[7] = new Vector3(xMin + (xMax-xMin)*1/4, yMin + (yMax-yMin)*1/2, rad);
		balls[8] = new Vector3(xMin + (xMax-xMin)*3/4, yMin + (yMax-yMin)*1/2, rad);
		*/
		balls[0] = new Vector3((int)(xMin + (xMax-xMin)/2), (int)(yMin + (yMax-yMin)/2), rad*2);
		for (int i = 1; i < balls.Length; i++)
			balls[i] = new Vector3(rand.Next((int)(xMin+rad), (int)(xMax-rad)), rand.Next((int)(yMin+rad), (int)(yMax-rad)), rad);

		Vector3 pos = new Vector3(xMin, yMin, 0);
		while (pos.x <= xMax && pos.y <= yMax)
		{
			float en = 0f;
			for(int b = 0; b < balls.Length; b++)
			{
				pos.z = balls[b].z;
				float radius = balls[b].z;
				float denom =  (float)Math.Max(0.0001, Math.Pow(Vector3.Distance(balls[b], pos), gooeyness));
				en += (radius / denom);
			}
			if(en > iso)
				fillCell(grid, (int)pos.x, (int)pos.y, upper);//, (byte)(upper - (upper-lower)*(en-iso)/en));
			pos.y += 1f;
			if (pos.y > yMax)
			{
				pos.x += 1f;
				pos.y = yMin;
			}
		}
	}
	
	private void makeIslandOld(Terrain.Cell[,][] grid, int xMin, int xMax, int yMin, int yMax, byte lower, byte upper)
	{
		int iSize = xMax - xMin;
		int jSize = yMax - yMin;
		double xRand = 0;//(!useRandomSeed)? 0 : rand.NextDouble()*xSize;
		double yRand = 0;//(!useRandomSeed)? 0 : rand.NextDouble()*ySize;
		double zRand = (!useRandomSeed)? 0 : rand.NextDouble();
		double densityThreshold = 0.5;


		int iStart = xMin + (iSize / 2);
		int jStart = yMin + (jSize / 2);

		for (int i = 0; i < iSize; i++)
		{
			for (int j = 0; j < jSize; j++)
			{
				for (byte k = lower; k < upper; k++)
				{
					if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0].upperBound >= k && grid[i, j][0].lowerBound <= k)
						continue;

					double xf = (double)(i-xMin)/iSize;
					double zf = (double)(j-yMin)/jSize;
					double yf = (double)(k-lower)/(double)(upper-lower);
					double xOffset = xMin + xRand;
					double yOffset = yMin + yRand;
					
					double plateau_falloff;
					if (yf >= 0.85)
						plateau_falloff = 0;
					else if (yf > 0.8 && yf < 0.85)
						plateau_falloff = 1.0 - (yf-0.8)*10.0;
					else
						plateau_falloff = 1.0 - (0.8-yf)*1.5;
					
					double center_falloff = 0.1/(
						Math.Pow((xf-0.5)*1.5, 2) +
						Math.Pow((yf-1.0)*0.8, 2) +
						Math.Pow((zf-0.5)*1.5, 2)
						);
					
					double density = (
						SimplexNoise.Generate(5, (float)(i+xf+xOffset)/(5f*iSize), (float)((yf*0.5f/5f)+zRand), (float)(j+zf+yOffset)/(5f*jSize)) *
						center_falloff *
						plateau_falloff
						);
					density *= Math.Pow(SimplexNoise.Generate((float)(i+xf+xOffset+1)/(10f*iSize), (float)(((yf+1)/10f)+zRand), (float)(j+zf+yOffset+1)/(10f*jSize))+0.4, 1.8);
					
					//if (SimplexNoise.Generate(5, (float)i/500f, (float)j/500f, (float)k/500f) > 2f)
					if (density >= densityThreshold)
					{
						fillCell(grid, i, j, k);
					}
				}
			}
		}
	}
	
	private void makeIslandOldRevise(Terrain.Cell[,][] grid, int xMin, int xMax, int yMin, int yMax, byte lower, byte upper)
	{
		int iSize = xMax - xMin;
		int jSize = yMax - yMin;
		double xRand = 0;//(!useRandomSeed)? 0 : rand.Next();
		double yRand = 0;//(!useRandomSeed)? 0 : rand.Next();
		double zRand = (!useRandomSeed)? 0 : rand.NextDouble()*1024;
		double densityThreshold = 0.5;
		double maxMidpointDensity = 0;
		while (maxMidpointDensity < densityThreshold)
		{
			xRand = 0;//(!useRandomSeed)? 0 : rand.Next();
			yRand = 0;//(!useRandomSeed)? 0 : rand.Next();
			zRand = (!useRandomSeed)? 0 : rand.NextDouble()*1024;

			for (int i = 0; i < upper-lower; i++)
			{
				double midpointDensity = getDensity(0.5, xRand, 0.5, yRand, (double)i/(upper-lower), zRand);
				if (midpointDensity > maxMidpointDensity)
					maxMidpointDensity = midpointDensity;
			}
		}
		if (densityThreshold > maxMidpointDensity)
			densityThreshold = maxMidpointDensity;
		
		int iStart = xMin + (iSize / 2);
		int jStart = yMin + (jSize / 2);
		
		for (int i = 0; i < iSize; i++)
		{
			for (int j = 0; j < jSize; j++)
			{
				for (byte k = lower; k < upper; k++)
				{
					if (grid[i, j] != null && grid[i, j].Length > 0 && grid[i, j][0].upperBound >= k && grid[i, j][0].lowerBound <= k)
						continue;
					
					double xf = (double)(i-xMin)/iSize;
					double zf = (double)(j-yMin)/jSize;
					double yf = (double)(k-lower)/(double)(upper-lower);
					double xOffset = xMin + xRand;
					double yOffset = yMin + yRand;
					
					/*double plateau_falloff;
					if (yf >= 0.85)
						plateau_falloff = 0;
					else if (yf > 0.8 && yf < 0.85)
						plateau_falloff = 1.0 - (yf-0.8)*10.0;
					else
						plateau_falloff = 1.0 - (0.8-yf)*1.5;
					
					double center_falloff = 0.1/(
						Math.Pow((xf-0.5)*1.5, 2) +
						Math.Pow((yf-1.0)*0.8, 2) +
						Math.Pow((zf-0.5)*1.5, 2)
						);
					
					double density = (
						SimplexNoise.Generate(5, (float)(i+xf+xOffset)/(5f*iSize), (float)((yf*0.5f/5f)+zRand), (float)(j+zf+yOffset)/(5f*jSize)) *
						center_falloff *
						plateau_falloff
						);
					density *= Math.Pow(SimplexNoise.Generate((float)(i+xf+xOffset+1)/(10f*iSize), (float)(((yf+1)/10f)+zRand), (float)(j+zf+yOffset+1)/(10f*jSize))+0.4, 1.8);
					//*/
					double density = getDensity(xf, xRand, yf, yRand, zf, zRand);
					
					//if (SimplexNoise.Generate(5, (float)i/500f, (float)j/500f, (float)k/500f) > 2f)
					if (density >= densityThreshold)
					{
						fillCell(grid, i, j, k);
					}
				}
			}
		}
	}

	private double getDensity(double xf, double xOffset, double yf, double yOffset, double zf, double zOffset)
	{
		double plateau_falloff;
		if (yf >= 0.85)
			plateau_falloff = 0;
		else if (yf > 0.8 && yf < 0.85)
			plateau_falloff = 1.0 - (yf-0.8)*10.0;
		else
			plateau_falloff = 1.0 - (0.8-yf)*1.5;
		
		double center_falloff = 0.1/(
			Math.Pow((xf-0.5)*1.5, 2) +
			Math.Pow((yf-1.0)*0.8, 2) +
			Math.Pow((zf-0.5)*1.5, 2)
			);
		
		double density = (
			SimplexNoise.Generate(5, (float)(xOffset + xf/5f), (float)(yOffset + yf/5f), (float)(zOffset + zf/5f)) *
			center_falloff *
			plateau_falloff
			);
		density *= Math.Pow(SimplexNoise.Generate((float)(xOffset + xf/10f), (float)(yOffset + yf/10f), (float)((zOffset + zf/10f)))+0.4, 1.8);

		return density;
	}
}
