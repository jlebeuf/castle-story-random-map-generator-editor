﻿using System;
using System.Collections;

public class ProceduralTerrainGenerator : ITerrainGenerator {

	public const byte DEFAULT_ISLAND_HEIGHT = 0x30;
	public const ushort DEFAULT_CENTER_ISLAND_RADIUS = 50;

	public ushort centerIslandRadius;


	public ProceduralTerrainGenerator()
	{
		centerIslandRadius = DEFAULT_CENTER_ISLAND_RADIUS;
	}
	
	public bool BuildTerrain(Terrain.Cell[,][] grid)
	{
		int xLength = grid.GetLength(0);
		int yLength = grid.GetLength(1);
		
		if (xLength < 512 || yLength < 512)
			return false;

		for (int i = 0; i < xLength; i++)
			for (int j = 0; j < yLength; j++)
				grid[i, j] = null;


		int xCenter = xLength/2;
		int yCenter = yLength/2;

		int iMin = xCenter-centerIslandRadius;
		int jMin = yCenter-centerIslandRadius;
		int iMax = xCenter+centerIslandRadius;
		int jMax = yCenter+centerIslandRadius;

		for (int i = xCenter-centerIslandRadius; i < iMax; i++)
		{
			for (int j = yCenter-centerIslandRadius; j < jMax; j++)
			{
				/*float distance = (float)Math.Sqrt(Math.Pow(xCenter-i, 2) + Math.Pow(yCenter-j, 2))/centerIslandRadius;
				float edge_falloff = 1f-(float)((9*Math.Pow(distance, 2))/10);
				float noise = SimplexNoise.Generate(10, (float)i, (float)j);
				float density = edge_falloff + (0.5f*noise);*/

				float xf = (float)(i-iMin)/(float)(iMax-iMin);
				float zf = (float)(j-jMin)/(float)(jMax-jMin);

				for (int k = 0; k < 100; k++)
				{
					float yf = (float)k/100f;

					float plateau_falloff;
					if (yf <= 0.6)
						plateau_falloff = 1.0f;
					else if (yf > 0.6 && yf < 0.9)
						plateau_falloff = 1.0f-(yf-0.8f)*10.0f;
					else
						plateau_falloff = 0.0f;
					
					float center_falloff = 0.1f/(float)(
						Math.Pow((xf-0.5)*1.5, 2) +
						Math.Pow((yf-1.0)*0.8, 2) +
						Math.Pow((zf-0.5)*1.5, 2)
						);
					float density = (
						SimplexNoise.Generate(5, xf, yf*0.5f, zf) *
						center_falloff *
						plateau_falloff
						);
					density *= (float)Math.Pow(
						SimplexNoise.Generate(5, (float)((xf+1)*3.0), (float)((yf+1)*3.0), (float)((zf+1)*3.0))+0.4, 1.8
						);

					if (density > 3.1)
					{
						if (grid[i, j] == null)
						{
							grid[i, j] = new Terrain.Cell[1];
							grid[i, j][0] = new Terrain.Cell();
							grid[i, j][0].upperBound = (byte)(k/5);
							grid[i, j][0].lowerBound = (byte)(k/5-1);
							grid[i, j][0].type = 0x02;
							grid[i, j][0].slope = 0x01;
						} else
						{
							if (grid[i, j][0].upperBound < (k/5))
								grid[i, j][0].upperBound = (byte)(k/5);
							if (grid[i, j][0].lowerBound > (k/5))
								grid[i, j][0].lowerBound = (byte)(k/5);
						}
					}
				}
			}
		}



		/*if (testFillType == TestFillType.Diamond)
		{
			// Diamond
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].lowerBound = (byte)(i + j);
					grid[i, j][0].upperBound = (byte)(i + j + 1);
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		} else
			if (testFillType == TestFillType.Bowl)
		{
			// Bowl
			for (int i = 0; i < xLength; i++)
			{
				for (int j = 0; j < yLength; j++)
				{
					grid[i, j] = new Terrain.Cell[1];
					grid[i, j][0] = new Terrain.Cell();
					grid[i, j][0].lowerBound = 0;
					grid[i, j][0].upperBound = (byte)(0.05f*(Mathf.Pow(i-(xLength/2), 2) + Mathf.Pow(j-(yLength/2), 2)));
					grid[i, j][0].type = 0x02;
					grid[i, j][0].slope = 0x01;
				}
			}
		} else
			if (testFillType == TestFillType.RandomGrid)
		{
			// RandomGrid
			new NoiseTerrainGenerator().BuildTerrain(grid);
		}*/
		
		return true;
	}
}
