﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour {
	
	public class LevelObject {
		public GameObject gameObject;
		public Transform transform;
		public Vector3 position;
	}

	private Terrain terrain;
	private GameObject terrainObject;
	private ITerrainGenerator activeGenerator = null;

	public int XLength { get { return terrain.XLength; } }
	public int YLength { get { return terrain.YLength; } }

	// Use this for initialization
	void Start () {
		terrain = FindObjectOfType<Terrain>();
		if (terrain == null)
			Debug.LogError("Terrain not found");
		else
			terrainObject = terrain.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setGenerator(ITerrainGenerator generator)
	{
		activeGenerator = generator;
	}

	public bool toggleTree(int x, int y, int z)
	{
		return terrain.toggleTree (x, y, z);
	}

	public void clearTrees()
	{
		terrain.clearTrees();
	}

	public void terreGrow(int x, int y, int z)
	{
		terrain.terreGrow(x, y, z);
	}

	public void terreShrink(int x, int y, int z, bool undercut)
	{
		terrain.terreShrink(x, y, z, undercut);
	}

	public void toggleTerreType(int x, int y, int z)
	{
		terrain.toggleTerreType(x, y, z);
	}

	public bool buildTerrain()
	{
		if (activeGenerator == null)
		{
			Debug.Log("Active generator is null");
			return false;
		} else
		if (terrain == null)
		{
			Debug.LogError("Terrain is null");
			return false;
		}

		terrain.clearTrees();
		return terrain.buildTerrain(activeGenerator);
	}

	public void resizeTerrain()
	{
		terrain.resize();
		terrain.clearTrees();
	}

	public void resizeTerrain(int newXLength, int newYLength, int xOffset, int yOffset)
	{
		terrain.resize(newXLength, newYLength, xOffset, yOffset);
		terrain.clearTrees();
	}

	public bool readLevel()
	{
		Terrain.Cell[,][] newGrid = MapReader.ReadTerrain();
		return terrain.setTerrain(newGrid);
	}

	public bool writeLevel()
	{
		bool terrainStatus = (MapWriter.WriteTerrain(terrain.Grid) == MapWriter.Status.Success);
		Terrain.Cell[,][] grid = terrain.Grid;
		int gridXLength = grid.GetLength(0);
		int gridYLength = grid.GetLength(1);
		bool[,] trees = terrain.Trees;
		int xLength = terrain.Trees.GetLength(0);
		int yLength = terrain.Trees.GetLength(1);
		byte[,] heights = new byte[xLength, yLength];
		for (int x = 0; x < xLength; x++)
		{
			for (int y = 0; y < yLength; y++)
			{
				if (x < gridXLength && y < gridYLength && grid[x, y] != null && grid[x, y].Length > 0 && grid[x, y][0] != null)
					heights[x, y] = grid[x, y][0].upperBound;
				else
					heights[x, y] = 0;
			}
		}
		bool treeStatus = MapWriter.WriteTrees(trees, heights);

		return (terrainStatus && treeStatus);
	}
}
